//import com.sun.xml.internal.fastinfoset.util.StringArray;
import gui.BoardVisualizer;
import gui.FlammeRougeBoard;
import tools.FileTools;
import tools.PartsToBuild;

import java.util.ArrayList;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

public class TrackVisualizer {

    public static void main(String[] args){
        RaceTrackGenerator raceGenerator = new RaceTrackGenerator();

        FlammeRougeBoard flammeRougeBoard = new FlammeRougeBoard();
        BoardVisualizer visual = new BoardVisualizer(flammeRougeBoard);

        String randomTrack = raceGenerator.getInitialTrack(raceGenerator);

        ArrayList<String> listOfTracks = RaceTrackGenerator.getRandomTrackList(raceGenerator, 1000);

        String fileToRead = "data/OnlyRightTurns.txt";
        //String fileToRead = "data/ExplorationTimeLapse.txt";
        //String fileToRead = "data/ExploredTrackTimelapse.txt";
        fileToRead = "/Users/davidgermain/Documents/Coding/boardgameAnalysis/flammeRouge/temp/trackStats/biggestBox_4-45_4-90_4-F.txt";
        ArrayList<String> myTracks = getTrackFromFile(fileToRead);

        //String rawTracksFile = "data/SideSwitchingExploration.txt";
        //ArrayList<String> myTracks = FileTools.readTrackFile(rawTracksFile);


        System.out.println("Read " + myTracks.size() + " tracks for timelapse");

        ArrayList<String> reversed = new ArrayList<>();
        for(int i= myTracks.size()-1; i>=0;i--){
            reversed.add(myTracks.get(i));
        }

        flammeRougeBoard.setTrack(randomTrack, raceGenerator.availableTracks);
        flammeRougeBoard.setNextTracks(reversed);



//        boolean saveImages = true;
//        if(saveImages) {
//            String folderOut = "/Users/davidgermain/Documents/BoardgameAnalysis/FlammeRouge/RightTurnTracks/";
//
//            try {
//                TimeUnit.SECONDS.sleep(5);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//
//            String filename = "a-000.png";
//            BoardVisualizer.saveImage(folderOut + filename);
//
//            for (int i = 1; i < 1000; i++) {
//                String nextTrack = flammeRougeBoard.getNextTrack();
//                if (nextTrack != null) {
//                    flammeRougeBoard.setTrack(nextTrack);
//                }
//
//                filename = "a-" + String.format("%04d", i) + ".png";
//                BoardVisualizer.saveImage(folderOut + filename);
//            }
//            //flammeRougeBoard.getNextTrack();
//        }

    }




    public static ArrayList<String> getTrackFromFile(String trackFile){

        ArrayList<String> tracks = FileTools.readTrackFile(trackFile);

        ArrayList<String> listOfTracks = new ArrayList<>();

        for(String track: tracks){
            String readTrack = "";
            //Process each track line to build track...
            String trackParts[] = track.split("-");
            System.out.println("track: " + track);
            PartsToBuild allParts = new PartsToBuild();
            for(String part:trackParts){
                //System.out.println("PArt: " + part);
                //Need to read each part, and associate to it a new track part...
                switch (part){
                    case "S":
                        readTrack += allParts.getStraight();
                        break;
                    case "R90":
                        readTrack += allParts.getLeft90(false);
                        break;
                    case "L90":
                        readTrack += allParts.getLeft90(true);
                        break;
                    case "R45":
                        readTrack += allParts.getLeft45(false);
                        break;
                    case "L45":
                        readTrack += allParts.getLeft45(true);
                        break;
                }
            }
            listOfTracks.add(readTrack);
        }

        return listOfTracks;
    }
}
