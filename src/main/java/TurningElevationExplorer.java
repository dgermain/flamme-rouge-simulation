import trackparts.TrackInfo;
import trackparts.TrackShape;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

//Here I want to explore how many different elevation profile it is possible to obtain
//Depending on a fixed number of left and right turns....

//So for a given Track, visit all permutation of track flipping them to fit the initial track shape
//And check how many elevation profile we get...

//So for a L-L-L-L-L-L


public class TurningElevationExplorer {

    RaceTrackGenerator trackGenerator;

    private int maxTurnTiles = 6;

    public static void main(String[] args ) {

        TurningElevationExplorer turnExplorer = new TurningElevationExplorer();
        turnExplorer.exploreAllCounts();
    }


    public TurningElevationExplorer() {
        trackGenerator = new RaceTrackGenerator(false, false, true);
    }

    public void exploreAllCounts() {

        ArrayList<Integer> elevationCounts = new ArrayList<>();

        for (int i = 0; i <= maxTurnTiles; ++i) {
            totalCount = 0;
            allElevation = new HashSet<>();
            elevationCounts.add(countElevationForRightTurns(i));
            System.out.println("Found " + totalCount + " different string for " + i + " right turns - Unique Elevation: " + allElevation.size());

        }

        System.out.println("Finished processing all turn count elevation");

       // System.out.println("Found " + totalCount + " different string");
    }

    public int countElevationForRightTurns(int rightTurns) {
        int elevationCount = 0;

        ArrayList<String> availableTrackPiece = new ArrayList<>();

        int rightTurnLeft = rightTurns;

        for(String k : trackGenerator.availableTracks.keySet()){
            String lower = k.toLowerCase();
            if(!availableTrackPiece.contains(lower))
                availableTrackPiece.add(lower);
        }

        buildTrack("", rightTurnLeft, availableTrackPiece);


        return elevationCount;
    }

    /***
     * Make sure to return the right turn version or the left turn version as needed.
     * @param turnRight
     * @param shapeToTest
     * @return
     */
    public String setTurnRightVersion(boolean turnRight, String shapeToTest) {

        String lowerCase = shapeToTest.toLowerCase();

        TrackInfo trackInfo = trackGenerator.getTrackInfo(Character.toString(lowerCase.charAt(0)));

        if (trackInfo.getTrackShape() == TrackShape.RIGHT_90 || trackInfo.getTrackShape() == TrackShape.RIGHT_45) {
            if (turnRight)
                return lowerCase;
            else
                return lowerCase.toUpperCase();
        } else {
            if (turnRight)
                return lowerCase.toUpperCase();
            else
                return lowerCase;
        }
    }

    int totalCount = 0;
    Set<String> allElevation = new HashSet<>();
    public void buildTrack(String trackToDate, int rightTurnsLeft, ArrayList<String> availableTrackPiece) {

        if (availableTrackPiece.size() == 0) {
            //System.out.println("We have a solution: " + trackToDate);
            String profile = trackGenerator.getElevationProfile(trackToDate);
            allElevation.add(profile);
           // System.out.println(trackToDate + " -> " + profile);
            totalCount++;
            return;
        }

        String nextTrack = trackToDate;
        for (int i = 0; i < availableTrackPiece.size(); ++i) {
            if (rightTurnsLeft > 0) {
                nextTrack = trackToDate + setTurnRightVersion(true, availableTrackPiece.get(i));
            } else {
                nextTrack = trackToDate + setTurnRightVersion(false, availableTrackPiece.get(i));
            }

            // trackToDate = trackToDate + availableTrackPiece.get(i);

            ArrayList<String> availableTrackPiecesLeft = (ArrayList<String>) availableTrackPiece.clone();
            availableTrackPiecesLeft.remove(i);

            buildTrack(nextTrack, rightTurnsLeft -1, availableTrackPiecesLeft);
        }
    }

}
