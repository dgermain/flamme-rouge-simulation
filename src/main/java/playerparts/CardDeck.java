package playerparts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.IntStream;

public class CardDeck {
    int[] unseenCount = new int[8];
    int[] seenCount = new int[9];

    //CardValues
    int cardValues[] = {2,3,4,5,6,7,8,9};

    ArrayList<Integer> unseenCards = new ArrayList<Integer>();
    ArrayList<Integer> seenCards = new ArrayList<Integer>();

    //Initialize deck of cards with given count...
    public void initializeDeck(int[] cardsCount){
        //Add each card to the deck
        for(int i=0; i< cardsCount.length;++i){
            for(int j=0;j<cardsCount[i];++j)
                unseenCards.add(cardValues[i]);
        }

        //Make sure the cards start shuffled
        Collections.shuffle(unseenCards);
    }

    public int[] getUnseenCount(){
        return countCards(unseenCards);
    }

    public int[] getSeenCount(){
        return countCards(seenCards);
    }

    //Get card Count.
    protected int[] countCards( ArrayList<Integer> deckCards){
        int[] cardCount = {0,0,0,0,0,0,0,0};
        for(int i=0; i < deckCards.size();++i){
            cardCount[deckCards.get(i) - 2]++;
        }
        return cardCount;
    }

    //Take all seen cards. Add them to unseen cards.
    //Reshuffle unseenCards.
    //empty seenCards
    protected void reshuffleSeenCards(){
        for(int i = 0; i< seenCards.size();++i){
            unseenCards.add(seenCards.get(i));
        }
        //Make sure deck is shuffled.
        Collections.shuffle(unseenCards);

        //If not cards are available, return a fatigue card instead...
        if(unseenCards.size() == 0)
            unseenCards.add(2);

        //Empty seen cards
        seenCards.clear();
    }

    public int[] drawCards(int drawSize){
        int cardDraw[] = {0,0,0,0};
        for(int i=0;i<drawSize;i++){
            if(unseenCards.size() <= 0){
                reshuffleSeenCards();
            }
            cardDraw[i] = unseenCards.get(0);
            unseenCards.remove(0);
        }

        return cardDraw;
    }

    public void returnCards(int[] returnedCards){
       int zeroCount =0;
        for(int i = 0; i < returnedCards.length; i++){
            if(returnedCards[i] > 0)
                seenCards.add(returnedCards[i]);
            else
                zeroCount++;
        }


        if(zeroCount < 1 || zeroCount > 1)
            System.out.println("Number of cards not returned: " + zeroCount);

    }

    public void addFatigueCard(){
        seenCards.add(2);
    }


}
