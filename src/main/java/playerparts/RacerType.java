package playerparts;

public enum RacerType {
    rouleur,
    sprinter;

    static int cardsDenomination[] = {2,3,4,5,6,7,8,9};
    static int rouleurCards[]      = {0,3,3,3,3,3,0,0};
    static int sprinterCards[]     = {3,3,3,3,0,0,0,3};
}
