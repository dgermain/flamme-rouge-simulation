import tools.*;
import trackparts.TrackShape;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static tools.Coordinates.getShiftVectorFloat;

/**
 * Created by davidgermain on 2018-02-06.
 */
public class ExhaustiveShapeExplorer {

    boolean keepTrackOfShapeStats = true;
    boolean checkCrossing = true;
    boolean saveTimelapse = false;

    TrackBuilderHelper builderHelper = new TrackBuilderHelper(1.0);

    LineCrossingChecker trackChecker;
    Stack<TrackShape> shapeStack;
    Stack<CoordinatesFloat> positionStack;

    double currentDirection = 0.0;
    CoordinatesFloat currentPos = new CoordinatesFloat(0,0);

    ShapeStatsKeeper runningStats = new ShapeStatsKeeper(6,6, 7, keepTrackOfShapeStats);

    long exploredStates = 0;
    long validCount = 0;
    int trackWidth = 64;

    int maxTurn90 = 6;
    int maxTurn45 = 6;
    int maxStraight = 7;

    int maxTotalSize = maxTurn90 + maxTurn45 + maxStraight +2;

    /***
     * useDouble assumes that mirror track images will result in the same validity.
     */
    BufferedWriter writer = null;
    boolean saveToFile = false;
    long expectedStates = 0;

    //Used to speed up angle calculation. 1 angle step being 22.5 degrees.
    private int currentAngleStep = 0;
    private static final int turn45_step = 2;
    private static final int turn90_step = 4;

    //0 = 0
    //1 = 22.5
    //2 = 45.0x
    //3 = ...
    //15 = 337.5

    static String baseSaveDir = "/Users/davidgermain/Documents/Projets/FlammeRouge/temp/";

    public static void main(String[] args ){
        //The optimization is compared here because in some case, the mirroring check is different
        //The turn use lines instead of curve crossing to check, so in some limit case, it causes a problem.
        int track45 = 6;
        int track90 = 6;
        int trackStraight = 7;

        String outputFolder = baseSaveDir;

        //Read command line arguments to determine number of pieces of each sort to use.
        if (args.length > 0) {
            try {
                track45 = Integer.parseInt(args[0]);
                track90 = Integer.parseInt(args[1]);
                trackStraight = Integer.parseInt(args[2]);
            } catch (NumberFormatException e) {
                System.err.println("Arguments 0 1 2" + args[0] + " "+args[1] + " "+ args[2] + " must be integer.");
                System.exit(1);
            }
        }
        //Read output directory to use.
        if (args.length > 3) {
            outputFolder = args[3];
            System.out.println("Saving in: " + outputFolder);
        }

        //Use first turn mirroring optimization
        boolean useFirstTurnMirrorOptimization = true;
        ExhaustiveShapeExplorer explorer = new ExhaustiveShapeExplorer(useFirstTurnMirrorOptimization, track45, track90, trackStraight, outputFolder);
    }


    public ExhaustiveShapeExplorer(boolean useFirstTurnMirrorOptimization, int track45, int track90, int trackStraight, String savingDir) {

        Date date = new Date();
        String formattedDate = new SimpleDateFormat("yyyyMMdd-HH-mm-ss").format(date);

        String suffix = "_" + maxTurn45+"-45_" + maxTurn90 + "-90_" + maxStraight + "-F.txt";
        String execLog = savingDir + "Execution-" + formattedDate + "-" + suffix;
        File execLogFile= new File(execLog);
        BufferedWriter execLogWriter = null;

        try {
            execLogWriter = new BufferedWriter(new FileWriter(execLogFile));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //For each segment we consider three lines (Left, Center, Right).
        //The shift vector allows to calculate the side lane position based on the center point.
        CoordinatesFloat shiftVector = getShiftVectorFloat((trackWidth /2.0), currentDirection);

        CoordinatesFloat rightStart = currentPos.add(shiftVector);
        CoordinatesFloat leftStart  = currentPos.subtract(shiftVector);

        //This will keep track of all segments and check if they intersect.
        trackChecker  = new LineCrossingChecker(rightStart,leftStart);
        positionStack = new Stack<>();
        shapeStack    = new Stack<>();

        //Keeping track of the center line position start of each segment
        positionStack.push(new CoordinatesFloat(0,0));

        //The first segment with the Start Line is always a straight segment
        boolean valid = checkValidity(TrackShape.STRAIGHT);

        //Keep track of how many complete race track are valid.
        validCount = 0;

        // Set Lower Number here for testig purpose...
        maxTurn90 = track90;
        maxTurn45 = track45;
        maxStraight = trackStraight;

        runningStats.setMaxTurns(maxTurn45,maxTurn90, maxStraight);

        maxTotalSize = maxTurn90 + maxTurn45 + maxStraight + 2;

        //long expected = (factorial(maxTurn90+maxTurn45+maxStraight) / (factorial(maxTurn90) * factorial(maxTurn45)* factorial(maxStraight))) * (int)Math.pow(2,maxTurn90+maxTurn45);


        long expected = (factorial(maxTurn90+maxTurn45+maxStraight) / (factorial(maxTurn90) * factorial(maxTurn45)* factorial(maxStraight))) * (int)Math.pow(2,maxTurn90+maxTurn45);

        printAndSave(execLogWriter, "Expected: " + expected);

        long expectedTest = (factorial(maxTurn90+maxTurn45+maxStraight) / (factorial(maxTurn90) * factorial(maxTurn45)* factorial(maxStraight))) ;
        printAndSave(execLogWriter, "Expected: " + expectedTest);


        expectedStates = expected;
        long startTime = System.currentTimeMillis();

        String savedFile = savingDir + (useFirstTurnMirrorOptimization?"noDouble":"withDouble") + "-Track-String.txt";
        File logFile=new File(savedFile);


        if(saveToFile) {
            try {
                writer = new BufferedWriter(new FileWriter(logFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        long count = explore(maxTurn45,maxTurn90,maxStraight, useFirstTurnMirrorOptimization);

        long endTime = System.currentTimeMillis();

        if(saveToFile) {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String statsFolder = savingDir + "trackStats/";
        runningStats.saveRunningStats(statsFolder);

        printAndSave(execLogWriter, "Total Valid count: " + validCount);
        printAndSave(execLogWriter, "Total count: " + count);
        printAndSave(execLogWriter, "That took " + (endTime - startTime)/1000.0 + " seconds");

        runningStats.printRunningStats();

        double rate = expected/((endTime - startTime)/1000.0);
        printAndSave(execLogWriter, "Or: " + rate + " per seconds");


        Long valueOf = count;
        double ratio = (double)valueOf/(double)expected;

        printAndSave(execLogWriter, "\n---->>>> Valid State ratio: " + ratio);


        maxTurn90 = 6;
        maxTurn45 = 6;
        maxStraight = 7;

        long expectedTotal = (factorial(maxTurn90+maxTurn45+maxStraight) / (factorial(maxTurn90) * factorial(maxTurn45)* factorial(maxStraight))) * (int)Math.pow(2,maxTurn90+maxTurn45);

        double days = expectedTotal * ratio /rate / 3600 /24;

        printAndSave(execLogWriter, "Full computations: " + days + " days for " + expectedTotal);
        printAndSave(execLogWriter, "\n---->>>> Explored States: " + exploredStates);

        try {
            execLogWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void printAndSave(BufferedWriter logWriter, String text) {
        System.out.println(text);
        try {
            logWriter.write(text +"\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static long factorial(int number) {
        long result = 1;

        for (int factor = 2; factor <= number; factor++) {
            result *= factor;
        }

        return result;
    }

    private boolean isThisTheFirstTurningTile(int turn90,int turn45){
        return (turn90 == maxTurn90) && (turn45 == maxTurn45);
    }

    long timelapseCount = 0;

    //In case we want to skip mirror track, here we only consider mirroring based on first turn...
    //So this cuts the total explored track in half, but not more...
    private long explore(int turn45Available, int turn90Available, int straightAvailable, boolean useFirstTurnMirrorOptimization){

//        if(shapeStack.size() + turn45Available +turn90Available +straightAvailable > maxTotalSize){
//            System.out.println("To investigate...");
//        }

        long count = 0;
        exploredStates++;


        //Here I want to print the actual track to create a timelapse of exploration with the first 1000 or full or partial track...
//        if(saveTimelapse) {
//            System.out.println("Timelapse\t" + trackToString());
//
//            if(shapeStack.size() == maxTotalSize - 1){
//                System.out.println("Timelapse\t" + trackToString() + "S");
//            }
//            if(exploredStates > 1000){
//                return -1;
//            }
//        }

        if(exploredStates % (expectedStates / 1000) == 0) {
            System.out.println((double)exploredStates / (double)expectedStates * 100  + "%");
        }

        //If this is the last piece of track. This is the finish line
        //Add a straight piece, and check that the track is still valid.
        if(shapeStack.size() == maxTotalSize - 1){

            if(saveTimelapse) {
                //System.out.println("Timelapse\t" + trackToString());

                //if(shapeStack.size() == maxTotalSize - 1){
                    System.out.println("Timelapse\t" + trackToString() + "S");
                //}
                if(timelapseCount > 1000){
                    return -1;
                }
                timelapseCount++;
            }

            if(checkValidity(TrackShape.STRAIGHT)){

                //System.out.println("Right Turns\t" + trackToString());
                if(saveToFile) {
                    try {
                        writer.write(trackToString() + "\n");

                        if (useFirstTurnMirrorOptimization) {
                            writer.write(invertedTrackToString() + "\n");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                validCount++;
                count++;

                runningStats.addTrack(shapeStack, trackChecker, trackToString());

                //Prepare for exploring next step
                removeLastShapeFromStack();

            }
        }else {
            for (TrackShape shape : TrackShape.values()) {

                int turn45 = turn45Available;
                int turn90 = turn90Available;
                int straight = straightAvailable;
                boolean doubleCount = false;

                //Try adding shape if they are available.
                switch (shape){
                    case LEFT_45:
                        //Added to speed up search of only right turns
                        //continue;
                    case RIGHT_45:
                        if (turn45Available <= 0)
                            continue;

                        //Special case, first turn count double, with mirror image
                        if (useFirstTurnMirrorOptimization && isThisTheFirstTurningTile(turn90, turn45)) {
                            //Skipping the left turn. But the Right will be counted double.
                            if (shape == TrackShape.LEFT_45)
                                continue;

                            doubleCount = true;
                        }

                        turn45 = turn45Available - 1;
                        break;
                    case LEFT_90:
                        //Added to speed up search of only right turns
                        //continue;
                    case RIGHT_90:
                        if (turn90Available <= 0)
                            continue;

                        //Special case, first turn count double. With mirror image
                        if(useFirstTurnMirrorOptimization && isThisTheFirstTurningTile(turn90,turn45)) {
                            //Skipping the left turn. But the Right will be counted double.
                            if (shape == TrackShape.LEFT_90)
                                continue;

                            doubleCount = true;
                        }

                        turn90 = turn90Available - 1;
                        break;

                    case STRAIGHT:
                        if (straightAvailable <= 0)
                            continue;

                        straight = straightAvailable-1;
                        break;
                }


                //The shape will be added in check Validity call if it appear valid.
                if (checkValidity(shape)) {

                    boolean useDoubleForTheRest = useFirstTurnMirrorOptimization && !doubleCount;
                    long localCount = explore(turn45, turn90, straight, useDoubleForTheRest);
                    if(localCount < 0)
                        return -1;
                    count += localCount;

                    //Since we assume the same validity of mirror track. Add twice the valid number of track.
                    if(doubleCount)
                        count += localCount;
                } else {
                    continue;
                }
            }

        }

        //Here we should drop the last stacked piece, everything was explored for this stage.
        removeLastShapeFromStack();

        //Show track with removed piece to show backtracking... ??? To compare with and without...
//        if(saveTimelapse) {
//            System.out.println("Timelapse\t" + trackToString());
//        }

        return count;
    }


    /***
     * This function serve to pop the last shape from the stack
     * It updates the current position and orientation to allow trying new shape in it's place.
     */
    private void removeLastShapeFromStack() {
        TrackShape shape = shapeStack.pop();

        positionStack.pop();
        currentPos = positionStack.lastElement();

        currentDirection -= builderHelper.shapeChangeDir.get(shape);
        currentAngleStep = (16 + currentAngleStep - builderHelper.angleStepChangeDir.get(shape)) & 15;

        if(checkCrossing) {
            trackChecker.dropEndPoint();

            //Drop second point for 45 degrees turns...
            //if (shape == TrackShape.LEFT_45 || shape == TrackShape.RIGHT_45) {

            //Drop second points for turns as well
            if(shape != TrackShape.STRAIGHT){
                trackChecker.dropEndPoint();
            }
        }
    }


    /**
     *
     * @param shape Shape to add to the current stack
     * @return was the shape added to the stack?
     *
     * This function fetch end points of a shape, based on current position and orientation.
     * It adds the shape to the stack if valid and not crossing any shape placed prior to that.
     */
    private boolean checkValidity(TrackShape shape) {

        CoordinatesFloat[] nextPointsLeft  = builderHelper.nextPtLeftFast[shape.ordinal()][currentAngleStep];
        CoordinatesFloat[] nextPointsRight = builderHelper.nextPtRightFast[shape.ordinal()][currentAngleStep];

        boolean valid = trackChecker.addIfValid(currentPos.add(nextPointsRight[0]), currentPos.add(nextPointsLeft[0]));

        //Add shape points if more than one point to be added.
        if(valid && nextPointsLeft.length > 1){
            valid = trackChecker.addIfValid(currentPos.add(nextPointsRight[1]), currentPos.add(nextPointsLeft[1]));

            //Don't forget to drop first point of shape, if we failed to add the second point!!!
            if(!valid){
                trackChecker.dropEndPoint();
            }
        }

        //Update current position and orientation.
        //Also add the shape to current stack of used shape, and position, since this is an exploration program
        // that will backtrack to try new shapes...
        if(valid){
            currentPos = currentPos.add(builderHelper.shapeEndPointFast[shape.ordinal()][currentAngleStep]);
            shapeStack.push(shape);
            positionStack.push(currentPos);

            currentAngleStep = (16 + currentAngleStep + builderHelper.angleStepChangeDir.get(shape)) & 15;
        }

        return valid;
    }


    private String invertedTrackToString() {
        StringBuilder roadStr = new StringBuilder(86);
        for (TrackShape shape : shapeStack) {
            switch (shape) {
                case STRAIGHT:
                    roadStr.append("S-");
                    break;
                case LEFT_90:
                    roadStr.append("R90-");
                    break;
                case RIGHT_90:
                    roadStr.append("L90-");
                    break;
                case LEFT_45:
                    roadStr.append("R45-");
                    break;
                case RIGHT_45:
                    roadStr.append("L45-");
                    break;
            }
        }

        return roadStr.toString();
    }


    private boolean checkRareLoop() {

        int consecutive90R = 0;
        int consecutive90L = 0;

        for(TrackShape shape:shapeStack){
            if(shape == TrackShape.LEFT_90)
                consecutive90L++;
            else
                consecutive90L = 0;
            if(shape == TrackShape.RIGHT_90)
                consecutive90R++;
            else
                consecutive90R = 0;

            if(consecutive90L == 3 || consecutive90R ==3)
                return true;
        }

        return false;
    }


    private String trackToString() {
        StringBuilder roadStr = new StringBuilder(86);
        if(shapeStack.size() > 21)
            System.out.println("Track To String - race track exceed 21 length:" + shapeStack.size());
        for(TrackShape shape: shapeStack){
            switch (shape){
                case STRAIGHT:
                    roadStr.append("S-");
                    break;
                case LEFT_90:
                    roadStr.append("L90-");
                    break;
                case RIGHT_90:
                    roadStr.append("R90-");
                    break;
                case LEFT_45:
                    roadStr.append("L45-");
                    break;
                case RIGHT_45:
                    roadStr.append("R45-");
                    break;
            }
        }

        return roadStr.toString();
    }

}
