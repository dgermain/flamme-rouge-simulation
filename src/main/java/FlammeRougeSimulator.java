import Player.PlayerInstance;
import Player.RandomPlayer;
import playerparts.RacerType;
import trackparts.RoadType;

import java.util.ArrayList;

/**
 * Created by davidgermain on 2018-01-13.
 * This is a full game simulator, that allows to build bot polaying agaist each other, or to
 * play against them as a human.
 */
public class FlammeRougeSimulator {

    class Cyclist{
        int playerId = 0;
        RacerType racerType;
    }

    //Contains the right and left side of the track.
    //Values are 0 if no one is there.
    //Value is playedId if cyclist is there.

    ArrayList<Cyclist> rightTrack = new ArrayList<>();
    ArrayList<Cyclist> leftTrack  = new ArrayList<>();

    ArrayList<RoadType> typeOfRoad = new ArrayList<>();
    PlayerInstance players[];


    public void playGame(){
        //Generate Race Track
        //Generate 2-4 players
        int nmbPlayers = 4;

        players = new PlayerInstance[nmbPlayers];

        for(int i=0;i < nmbPlayers; ++i)
            players[i] = new RandomPlayer();

        //Create Racer Positions
        //

        //Start Game.
        selectStartingPosition();

        while(true){
            if(isGameFinished())
                break;

            playTurn();
        }
    }

    private boolean isGameFinished(){
        //Check if player finished

        return true;
    }

    private void selectStartingPosition(){
        //Let all player select their starting position.
    }

    private void playTurn(){
        //Ask player to select cards.
        //for(int i=0; i<
        for(PlayerInstance player: players){
            player.selectCards();
        }

        //Advance racer from front to back.
        for(int i = 0; i<  rightTrack.size();++i){
            int pos = rightTrack.size() - i - 1;
            Cyclist frontCyclist = rightTrack.get(pos);
            if(frontCyclist != null){
                int move = players[frontCyclist.playerId].getCyclistCard(frontCyclist.racerType);

                //Need to advance cyclist of move #

                Cyclist sideCyclist = leftTrack.get(pos);
                if(sideCyclist != null) {
                    int sideMove = players[sideCyclist.playerId].getCyclistCard(sideCyclist.racerType);
                    //Need to advance SideCyclist of move #
                }
            }
        }

        //Apply drafting from back to front

        //Distribute fatigue cards

    }

}
