import gui.BoardVisualizer;
import gui.FlammeRougeBoard;
import tools.FileTools;
import trackparts.RaceTrackReader;
import trackparts.RoadType;
import trackparts.TrackInfo;
import trackparts.TrackShape;

import java.util.*;
import java.util.Random;

/**
 * Created by davidgermain on 2018-01-13.
 */
public class RaceTrackGenerator {

    Random rnd = new Random();
    Map<String, TrackInfo> availableTracks = new HashMap<>();
    //Here I want to generate a track
    //Evaluate if a track is valid (not crossing itself)
    //I would like to evaluate track equivalence...
    // Find a way to score it's difficulty!!

    public RaceTrackGenerator(){
        readAvailableTracks();
    }

    public RaceTrackGenerator(boolean includeStraight, boolean include45, boolean include90) {
        readAvailableTracks();

        //Remove non-wanted shape in track for evaluation purpose...
        ArrayList<String> toRemove = new ArrayList<>();
        for(String k : availableTracks.keySet()){
            TrackShape shape = availableTracks.get(k).getTrackShape();
            if(!include45 && (shape == TrackShape.LEFT_45 || shape == TrackShape.RIGHT_45)){
                toRemove.add(k);
            }
            else if(!include90 && (shape == TrackShape.LEFT_90 || shape == TrackShape.RIGHT_90)){
                toRemove.add(k);
            }
            else if(!includeStraight && shape == TrackShape.STRAIGHT){
                toRemove.add(k);
            }

        }
        for(String s : toRemove){
            availableTracks.remove(s);
        }
    }

    public  TrackInfo getTrackInfo(String c){
        return availableTracks.get(c);
    }



    public static ArrayList<String> getRandomTrackList(RaceTrackGenerator raceGenerator, int size){
        ArrayList<String> listOfTracks = new ArrayList<>();
        for(int i=0;i<size;i++){
            listOfTracks.add(raceGenerator.generateRandomTrack());
        }

        return listOfTracks;
    }


     public String getInitialTrack(RaceTrackGenerator raceGenerator){

        Map<String, String> namedRaceTracks = new HashMap<>();

        namedRaceTracks.put("classicimo", "AebQRNHPcgikDFsLojmtu");
        namedRaceTracks.put("ballonCentral", "AnLHgceqtrMBoipjDFkSu");

        namedRaceTracks.put("knownCrossing", "agHPNRTQLBojkiFESDCMu");
        namedRaceTracks.put("outOfBound", "ahjcOdkenlqsrgpiTBFMu");
        namedRaceTracks.put("notCentered", "AFkOlGrmnqEPIBSCtJdhU");
        namedRaceTracks.put("roundProblem", "AsiegoU");

        namedRaceTracks.put("windingInsideProblem_A", "AoGmserkQpDU");
        namedRaceTracks.put("windingInsideProblemB", "aOgMSERKqPdu");

        namedRaceTracks.put("loopIssue", "AsjKGbDrEqpU");


        //S-S-S-L90-L45-R45-R90-R90-L45-R90-L45-S-
        String problem_001 = "abcPqrEjSoKD";

        String randomTrack = raceGenerator.generateRandomTrack();

        return randomTrack;
    }


    public String generateRandomTrack() {

        String randomized= null;

        ArrayList<String> trackIds = listAvailablePieces(availableTracks);

        String startId = findStart(trackIds,availableTracks);
        String finishId = findFinish(trackIds,availableTracks);

        if(startId != null && finishId != null) {
            trackIds.remove(startId);
            trackIds.remove(finishId);

            randomized = randomizeTrack(startId, finishId, trackIds);
        }else {
            //Special case ?
            randomized = randomizeTrack(startId, finishId, trackIds);
        }

        return randomized;
    }

    public void readAvailableTracks(){

        RaceTrackReader tracksReader = new RaceTrackReader("data/racetrackComponents.txt");//"/Users/davidgermain/IdeaProjects/flammerougesimulator/data/racetrackComponents.txt");

        availableTracks = tracksReader.getUsableTracks();
    }

    private String randomizeTrack(String startId, String finishId, ArrayList<String> trackIds) {
        String finalTrack = "";

        if(startId != null) {
            finalTrack += doFlipTrack() ? startId.toLowerCase() : startId.toUpperCase();
        }

        //Add all track here...
        Collections.shuffle(trackIds);
        for(String s:trackIds){
            finalTrack += doFlipTrack() ? s.toLowerCase() : s.toUpperCase();
        }

        if(finishId != null) {
            finalTrack += doFlipTrack() ? finishId.toLowerCase() : finishId.toUpperCase();
        }

        return finalTrack;
    }

    private boolean doFlipTrack() {
        return rnd.nextBoolean();
    }

    private String findStart(ArrayList<String> trackIds, Map<String, TrackInfo> allTracks) {
        for(String s : trackIds){
            if(allTracks.get(s).isStart()) {
                return s;
            }
        }

        return null;
    }

    private String findFinish(ArrayList<String> trackIds, Map<String, TrackInfo> allTracks) {
        for(String s : trackIds){
            if(allTracks.get(s).isFinish()) {
                return s;
            }
        }

        return null;
    }

    private ArrayList<String> listAvailablePieces(Map<String, TrackInfo> allTracks) {
        Set<String> tileIds = new HashSet<>();

        ArrayList<String> availableIds = new ArrayList<>();

        for(String s: allTracks.keySet()){
            tileIds.add(s.toLowerCase());
        }


        for(String s: tileIds){
            availableIds.add(s);
        }

        return availableIds;
    }


    public String getElevationProfile(String raceString){
        ArrayList<RoadType> segmentType = new ArrayList<>();
        ArrayList<Integer> segmentLength = new ArrayList<>();

        int roadTotalLength = 0;
        for(char c: raceString.toCharArray()){
            TrackInfo info = getTrackInfo(Character.toString(c));
            //System.out.println(info.toString());

            for(int i=0; i< info.getTypeSize(); ++i){
                RoadType roadType = info.getRoadType(i);
                int roadLength = info.getTrackSegmentLength(i);

                //System.out.println("Segment: " + c + " Type: " + roadType.toString());

                if(segmentType.isEmpty() || segmentType.get(segmentType.size()-1) != roadType){
                    segmentType.add(roadType);
                    segmentLength.add(roadLength);
                }else {
                    segmentLength.set(segmentType.size()-1, roadLength + segmentLength.get(segmentLength.size()-1));
                }
                roadTotalLength+=roadLength;
            }
        }

        String elevationProfileStr = roadToString(segmentType, segmentLength);

        return elevationProfileStr;
    }

    private String roadToString(ArrayList<RoadType> segmentType, ArrayList<Integer> segmentLength) {
        String roadStr = "";
        for(int i=0 ; i< segmentType.size();++i){
            roadStr += segmentLength.get(i);

            switch(segmentType.get(i)){
                case PRESTART:
                    roadStr += "S"; break;
                case UP:
                    roadStr += "U"; break;
                case DOWN:
                    roadStr += "D"; break;
                case FLAT:
                    roadStr += "F"; break;
                case FINISH:
                    roadStr += "E"; break;

                    default:
                        roadStr += "?"; break;

            }
        }

        return roadStr.substring(0,roadStr.length());
    }
}
