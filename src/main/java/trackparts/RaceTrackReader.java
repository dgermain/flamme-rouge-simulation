package trackparts;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by davidgermain on 2018-01-15.
 */
public class RaceTrackReader {


    private Map<String, TrackInfo> usableTracks = new HashMap<>();

    public Map<String, TrackInfo> getUsableTracks() {
        return usableTracks;
    }

    public static void main(String[] args){
        RaceTrackReader raceTrack = null;


        raceTrack = new  RaceTrackReader("data/racetrackComponents.txt");//"/Users/davidgermain/IdeaProjects/flammerougesimulator/data/racetrackComponents.txt");


        for(String segmentId: raceTrack.usableTracks.keySet()){
            System.out.println(segmentId + " -> " + raceTrack.usableTracks.get(segmentId).toString());
        }
    }

    public  RaceTrackReader(String trackFile) {
        readFile(trackFile);
    }


    private void readFile(String trackFile){
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(trackFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String line = null;
        try {
            line = br.readLine();


            while (line != null) {
                line = line.trim();

                //Skip comment, or empty line
                if(line.length() != 0) {
                    ExtractLine(line);
                }

                line = br.readLine();
            }

            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void ExtractLine(String line) {
        String[] components = line.split("\t");

        if(components.length != 3) {
            System.out.println("Malformed line in file: " + line);
        }
        else {
            String componentId = components[0];
            String componentShapeStr = components[1];

            TrackShape shape = TrackShape.STRAIGHT;

            switch (componentShapeStr) {
                case "straight":
                    shape = TrackShape.STRAIGHT;
                    break;
                case "90-left":
                    shape = TrackShape.LEFT_90;
                    break;
                case "90-right":
                    shape = TrackShape.RIGHT_90;
                    break;
                case "45-left":
                    shape = TrackShape.LEFT_45;
                    break;
                case "45-right":
                    shape = TrackShape.RIGHT_45;
                    break;
                default:
                    System.out.println("Error reading shape: " + componentShapeStr);

            }

            TrackInfo trackSegment = new TrackInfo(componentId, shape);

            String[] roadParts = components[2].split(",");

            //System.out.println("Read Part: " + componentId + " Shape: " + componentShapeStr + " Roads: " + components[2]);
            for(String part: roadParts){
                String[] comp = part.split("-");
                //System.out.println("\t" + comp[1] + " X " + comp[0]);
                RoadType road = RoadType.valueOf(comp[1].toUpperCase());
                int roadLength = Integer.valueOf(comp[0]);

                trackSegment.addRoadSegment(road, roadLength);
                //System.out.println(road.toString() + " " + roadLength);
            }

            usableTracks.put(componentId, trackSegment);
        }
    }
}
