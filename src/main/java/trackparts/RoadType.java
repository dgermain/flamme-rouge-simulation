package trackparts;

/**
 * Created by davidgermain on 2018-01-13.
 */
public enum RoadType {
    PRESTART,
    FLAT,
    UP,
    DOWN,
    FINISH
}
