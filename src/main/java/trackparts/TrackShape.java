package trackparts;

/**
 * Created by davidgermain on 2018-01-13.
 */
public enum TrackShape {
    STRAIGHT,
    LEFT_90,
    RIGHT_90,
    LEFT_45,
    RIGHT_45

}
