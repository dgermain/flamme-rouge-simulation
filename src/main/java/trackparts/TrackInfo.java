package trackparts;

import java.util.ArrayList;

/**
 * Created by davidgermain on 2018-01-13.
 */
public class TrackInfo {

    private String id;
    private TrackShape shape;
    private ArrayList<RoadType> roadType;
    private ArrayList<Integer> roadLength;

    private ArrayList<RoadType> roadTypeSegment;
    private int totalLength = 0;

    private boolean isStart = false;
    private boolean isFinish = false;

    public TrackInfo(String id, TrackShape shape){
        this.id = id;
        this.shape = shape;

        this.roadType   = new ArrayList<>();
        this.roadTypeSegment   = new ArrayList<>();
        this.roadLength = new ArrayList<>();

        isStart = false;
        isFinish = false;
        totalLength = 0;
     }

    //Copy Constructor -- Useful to create list of usable Tracks ????
    public TrackInfo(TrackInfo other){
        this.id = other.id;
        this.shape = other.shape;
        this.roadType = new ArrayList<>();

        for(RoadType type : other.roadType)
            this.roadType.add(type);

        for(Integer length : other.roadLength)
            this.roadLength.add(length);

        for(RoadType type : other.roadTypeSegment)
            this.roadTypeSegment.add(type);

        this.totalLength = other.totalLength;

        this.isFinish = other.isFinish;
        this.isStart = other.isStart;
    }

    public void addRoadSegment(RoadType roadTypeSegment, int lengthSegment){
        roadType.add(roadTypeSegment);
        roadLength.add(lengthSegment);
        totalLength += lengthSegment;

        for(int i=0; i< lengthSegment;++i){
            this.roadTypeSegment.add(roadTypeSegment);
        }

        if(roadTypeSegment == RoadType.FINISH){
            isFinish = true;
        }

        if(roadTypeSegment == RoadType.PRESTART){
            isStart = true;
        }
    }

    public int getTrackLength(){
        return totalLength;
    }

    public String getId(){
        return id;
    }

    public TrackShape getTrackShape(){
        return shape;
    }

    public int getTypeSize(){
        return roadType.size();
    }

    public RoadType getRoadType(int index){
        return roadType.get(index);
    }

    public RoadType getTrackSegmentType(int index){
        return roadTypeSegment.get(index);
    }

    public int getTrackSegmentLength(int index){
        return roadLength.get(index);
    }

    public boolean isStart() {
        return isStart;
    }

    public void setStart(boolean start) {
        isStart = start;
    }

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean finish) {
        isFinish = finish;
    }

    @Override
    public String toString() {
        return "TrackInfo{" +
                "id='" + id + '\'' +
                ", shape=" + shape +
                ", roadType=" + roadType +
                ", roadLength=" + roadLength +
                ", totalLength=" + totalLength +
                ", isStart=" + isStart +
                ", isFinish=" + isFinish +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TrackInfo)) return false;

        TrackInfo trackInfo = (TrackInfo) o;

        if (totalLength != trackInfo.totalLength) return false;
        if (isStart != trackInfo.isStart) return false;
        if (isFinish != trackInfo.isFinish) return false;
        if (!getId().equals(trackInfo.getId())) return false;
        if (shape != trackInfo.shape) return false;
        if (!roadType.equals(trackInfo.roadType)) return false;
        return roadLength.equals(trackInfo.roadLength);
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + shape.hashCode();
        result = 31 * result + roadType.hashCode();
        result = 31 * result + roadTypeSegment.hashCode();
        result = 31 * result + roadLength.hashCode();
        result = 31 * result + totalLength;
        result = 31 * result + (isStart ? 1 : 0);
        result = 31 * result + (isFinish ? 1 : 0);
        return result;
    }
}
