import tools.CoordinatesFloat;
import tools.HistogramBins;
import tools.LimitValues;
import tools.LineCrossingChecker;
import trackparts.TrackShape;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class ShapeStatsKeeper {

    private int maxTurn45 = 0;
    private int maxTurn90 = 0;
    private int maxStraight = 0;


    public void setMaxTurns(int max45, int max90, int maxFlat){
        maxTurn45 = max45;
        maxTurn90 = max90;
        maxStraight =  maxFlat;

        left_45_turnCount = new long[maxTurn45+1];
        left_90_turnCount = new long[maxTurn90+1];

        for(int i=0;i<=maxTurn45;++i){
            left_45_turnCount[i] = 0;
        }
        for(int i=0;i<=maxTurn90;++i){
            left_90_turnCount[i] = 0;
        }

        for(int i=0; i<7; i++){
            for(int j=0;j<7;j++){
                keep45and90LeftTurnCounts[i][j] = 0;
            }
        }
    }

    long[]  left_45_turnCount = new long[7];
    long[]  left_90_turnCount = new long[7];

    long[][] keep45and90LeftTurnCounts = new long[7][7];



    int spreadBinCount = 2000;
    double inferiorLimit = 0;
    double superiorLimit = 2000000;

    HistogramBins boxSizeHistogram = new HistogramBins(inferiorLimit,superiorLimit,spreadBinCount);
    HistogramBins longestAxisHistogram = new HistogramBins(0,3000, 3000);

    ArrayList<Long> minBoxSizeCount = new ArrayList<>();
    ArrayList<Long> longestAxisCount = new ArrayList<>();

    PriorityQueue<TrackPair> biggestSurface;
    PriorityQueue<TrackPair> smallestSurface;

    PriorityQueue<TrackPair> biggestLength;
    PriorityQueue<TrackPair> smallestLength;

    int keepTopN = 100;

    public ShapeStatsKeeper(int maxLeft45, int maxLeft90, int maxFlat, boolean keepShapeStats){
        initRunningStats(maxLeft45,maxLeft90,maxFlat, keepShapeStats);
    }

    int nmbRotation = 19; //19;
    long rotationBest[];
    boolean keepSpatialStats = true;
    void initRunningStats(int maxLeft45, int maxLeft90, int maxFlat, boolean doKeepSpatialStats){
        setMaxTurns(maxLeft45, maxLeft90, maxFlat);

        boolean biggest = true;
        boolean smallest = false;
        //int keepTopN =  500;
        biggestSurface = createPriorityQueue(keepTopN, biggest);
        smallestSurface = createPriorityQueue(keepTopN, smallest);

        biggestLength = createPriorityQueue(keepTopN, biggest);
        smallestLength = createPriorityQueue(keepTopN, smallest);
        keepSpatialStats = doKeepSpatialStats;

        rotationBest = new long[nmbRotation];
        for(int i=0;i<rotationBest.length;++i) {
            rotationBest[i] = 0;
        }

        initPlayTableLimits();
    }


    public void addTrack(Stack<TrackShape> shapeStack, LineCrossingChecker trackChecker, String trackStr) {

        int left45 = 0;
        int left90 = 0;

        for(TrackShape s: shapeStack){
            if(s == TrackShape.LEFT_45)
                left45++;
            else if(s==TrackShape.LEFT_90)
                left90++;
        }

        left_45_turnCount[left45]++;
        left_90_turnCount[left90]++;

        keep45and90LeftTurnCounts[left45][left90]++;
        keep45and90LeftTurnCounts[maxTurn45-left45][maxTurn90-left90]++;

        //Adding mirror count...
        left_45_turnCount[maxTurn45-left45]++;
        left_90_turnCount[maxTurn90-left90]++;

        if(keepSpatialStats) {
            findTrackSizeStats(shapeStack, trackChecker, trackStr);
        }
    }
    private final double cosAngle = Math.cos(Math.toRadians(5));
    private final double sinAngle = Math.sin(Math.toRadians(5));

    LimitValues getBestRotationValues(ArrayList<CoordinatesFloat> allCoord, double degree, int iteration){
        LimitValues baseValues = getLimitValues(allCoord);

        int bestTurn = 0;
        //Rotating around 0,0 point.
        //double angle = Math.toRadians(degree);
        //double cosAngle = Math.cos(angle);
        //double sinAngle = Math.sin(angle);

        for(int i=1; i< iteration; ++i){
            for(CoordinatesFloat c: allCoord) {

                double xRot = cosAngle * c.x - sinAngle * c.y;
                double yRot = sinAngle * c.x + cosAngle * c.y;
                c.x = xRot;
                c.y = yRot;
            }

            LimitValues rotatedValues = getLimitValues(allCoord);

            /*baseValues.getKey() > rotatedValues.getKey() &&*/
            //Update min box if it's smaller.
            baseValues.evaluateValues(rotatedValues, 2.0);
            //baseValues = new LimitValues(baseValues,rotatedValues,2.0);

        }

        return baseValues;
    }

    LimitValues getLimitValues(ArrayList<CoordinatesFloat> allCoord){
        double minX  = allCoord.get(0).x;
        double minY  = allCoord.get(0).y;
        double maxX = minX;
        double maxY = minY;

        for(CoordinatesFloat c: allCoord) {
            if (c.x < minX)
                minX = c.x;
            if (c.y < minY)
                minY = c.y;
            if (c.x > maxX)
                maxX = c.x;
            if (c.y > maxY)
                maxY = c.y;
        }

       return new LimitValues(maxX - minX, maxY - minY); //<>(surface, maxLength);
    }

    private void findTrackSizeStats(Stack<TrackShape> shapeStack, LineCrossingChecker trackChecker, String trackStr) {
        ArrayList<CoordinatesFloat> allCoord = trackChecker.getAllCoordinates();

        LimitValues bestValues = getBestRotationValues(allCoord, 5, nmbRotation);

        double minSurface = bestValues.getSurface();
        double maxLength = bestValues.getMaxLength();

        checkTableFit(bestValues, trackStr);

        TrackPair minSurfacePair = new TrackPair(trackStr, (int)Math.round(minSurface));
        add(keepTopN, biggestSurface, minSurfacePair);
        add(keepTopN, smallestSurface, minSurfacePair);

        TrackPair maxLengthPair = new TrackPair(trackStr, (int)Math.round(maxLength));

        add(keepTopN, biggestLength, maxLengthPair);
        add(keepTopN, smallestLength, maxLengthPair);

        boxSizeHistogram.addPoint(minSurface);
        longestAxisHistogram.addPoint(maxLength);
    }

    private int tableType = 7;
    private double[] tableSizeX = new double[tableType];
    private double[] tableSizeY = new  double[tableType];

    private long[] tableFit = new long[tableType];

    ArrayList<PriorityQueue<TrackPair>> allTableFit = new ArrayList<>();

    private void initPlayTableLimits() {

        for(int i=0; i<tableType;++i){
            tableFit[i] = 0;
            //(true is to keep the largest tracks fitting on a table.)
            allTableFit.add(createPriorityQueue(200, true));
        }
        //Square table can be all taken from the Max Axis Length Histogram!!!!

        //Table 0 - square table card 28 x 43 pouces - 86 cm
        tableSizeX[0] = 711; //(in mm)
        tableSizeY[0] = 1092; //(in mm)

        //26x48 ???? Coffee Table (660 x 1219) in mm
        tableSizeX[1] = 660; //(in mm)
        tableSizeY[1] = 1219;

        //90x120cm
        tableSizeX[2] = 900; //(in mm)
        tableSizeY[2] = 1200;

        //34 x 60cm
        tableSizeX[3] = 864; //(in mm)
        tableSizeY[3] = 1524;

        //34 x 80 inch
        tableSizeX[4] = 864; //(in mm)
        tableSizeY[4] = 2032;

        //38 x 72
        tableSizeX[5] = 965; //(in mm)
        tableSizeY[5] = 1828;

        tableSizeX[6] = 1066; //(in mm)
        tableSizeY[6] = 2438;
        //avg width  34 inches to 42 inches 864 - 1066
        //avg Length 60 -> 96 inches 1524 - 2438


        //Diner Table Small -
        //Diner Table medium -
        //Diner Table large -

    }

    private void checkTableFit(LimitValues minSurface, String track) {
        boolean added = false;
        for(int i=0; i < tableType;++i){
            if(minSurface.fitInBox(tableSizeX[i],tableSizeY[i])) {
                tableFit[i]++;

                //Only add For table if it fix in box.
                //if(!added) {
                    add(200, allTableFit.get(i), new TrackPair(track, (int) Math.round(minSurface.getSurface())));
                    added = true;
                //}

            }
        }
    }

    public static class TrackPair{

        public TrackPair(String track, int score){
            trackScore = score;
            trackString = track;
        }
        String trackString = "";
        int trackScore = 0;
    }

    public void saveRunningStats(String folderOut) {
        String suffix = "_" + maxTurn45+"-45_" + maxTurn90 + "-90_" + maxStraight + "-F.txt";
        String smallestBoxFile = folderOut + "smallestBox" + suffix;
        String biggestBoxFile = folderOut + "biggestBox" + suffix;
        String shortestLengthFile = folderOut + "shortestLength" + suffix;
        String longestLengthBoxFile = folderOut + "longestLength" + suffix;

        if(keepSpatialStats) {
            writePriorityQueueToFile(smallestSurface, smallestBoxFile);
            writePriorityQueueToFile(biggestSurface, biggestBoxFile);

            writePriorityQueueToFile(smallestLength, shortestLengthFile);
            writePriorityQueueToFile(biggestLength, longestLengthBoxFile);
        }

        for(int i=0; i< allTableFit.size();++i) {
            String tableSizeFileName =  folderOut + "TableFit_" + i + "X_"+ tableSizeX[i] + "Y_" + tableSizeY[i] + "_BestFitting.txt";
            writePriorityQueueToFile(allTableFit.get(i), tableSizeFileName);
        }

        String turn45File = folderOut + "45_degreesTurnCount_" + suffix;
        String turn90File = folderOut + "90_degreesTurnCount_" + suffix;

        String combinedTurnFile = folderOut + "combined_degreesTurnCount_" + suffix;

        writeTurnCount(left_45_turnCount, turn45File);
        writeTurnCount(left_90_turnCount, turn90File);
        writeCombinedTurnCount(keep45and90LeftTurnCounts, combinedTurnFile);

        //Write histogram to file
        String boxSizeHistoFile = folderOut + "boxSizeHistogram" + suffix;
        String longestAxisHistoFile = folderOut + "longestAxisHistogram" + suffix;

        longestAxisHistogram.writeToFile(longestAxisHistoFile);
        boxSizeHistogram.writeToFile(boxSizeHistoFile);

        String tableFitFile = folderOut + "TableFit" + suffix;

        writeTableFitToFile(tableFitFile);
    }

    private void writeCombinedTurnCount(long[][] keep45and90LeftTurnCounts, String combinedTurnFile) {

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(combinedTurnFile));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            writer.write("left 90 -> " + "\t");
            for(int j=0;j<7;j++){
                writer.write(j + "\t");
            }
            writer.write("\n");

            for(int i=0; i<7; i++){
                writer.write("left 45: " + i + "\t");
                for(int j=0;j<7;j++){
                    writer.write(keep45and90LeftTurnCounts[i][j] + "\t");
                }
                writer.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void writeTableFitToFile(String filename){
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            for(int i=0;i<tableFit.length;++i){
                writer.write(i + " Table Fit (  " + tableSizeX[i] + "," + tableSizeY[i] +  " ) Count: " + tableFit[i] + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void printRunningStats() {
        //Save to file...

        for(TrackPair p: biggestLength){
            System.out.println("biggest Length : " + p.trackScore + " for " + p.trackString);
        }
        System.out.println("");

        for(TrackPair p: smallestLength){
            System.out.println("smallest Length : " + p.trackScore + " for " + p.trackString);
        }
        System.out.println("");

        for(TrackPair p: biggestSurface){
            System.out.println("biggest Surface : " + p.trackScore + " for " + p.trackString);
        }
        System.out.println("");
        for(TrackPair p: smallestSurface){
            System.out.println("smallest Surface : " + p.trackScore + " for " + p.trackString);
        }

        System.out.println("");
        for(int i=0;i<=maxTurn45;++i){
            System.out.println(i + " Left 45 turns: " + left_45_turnCount[i]);
        }
        System.out.println("");
        for(int i=0;i<=maxTurn90;++i){
            System.out.println(i + " Left 90 turns: " + left_90_turnCount[i]);
        }

        //for(int i=0; i<7; i++){
            System.out.print("left 90 -> " + "\t");
            for(int j=0;j<7;j++){
                System.out.print(j + "\t");
            }
            System.out.print("\n");
        //}
        for(int i=0; i<7; i++){
            System.out.print("left 45: " + i + "\t");
            for(int j=0;j<7;j++){
                System.out.print(keep45and90LeftTurnCounts[i][j] + "\t");
            }
            System.out.print("\n");
        }
        //System.out.println("");
        //for(int i=0;i<rotationBest.length;++i){
        //    System.out.println(i + " Best Rotation Score: " +rotationBest[i]);
        //}
        System.out.println("");
        for(int i=0; i< tableType;++i){
            System.out.println(i + " Table Fit (  " + tableSizeX[i] + "," + tableSizeY[i] +  " ) Count: " + tableFit[i]);
        }
    }


    private void writeTurnCount(long[] turnCountArray, String endfile){
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(endfile));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            for(int i=0; i< turnCountArray.length;++i){
                writer.write(i + "\t" + turnCountArray[i] + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void writePriorityQueueToFile(PriorityQueue<TrackPair> dataToSave, String endfile){
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(endfile));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            for(TrackPair p: dataToSave){
                writer.write(p.trackScore + "\t" + p.trackString + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public PriorityQueue<TrackPair> createPriorityQueue(int keepBestN, boolean biggest){
        if(!biggest) {
            return new PriorityQueue<TrackPair>(keepBestN, new Comparator<TrackPair>() {
                @Override
                public int compare(TrackPair o1, TrackPair o2) {
                    return o2.trackScore - o1.trackScore;
                }
            });

        }else {
            return new PriorityQueue<TrackPair>(keepBestN, new Comparator<TrackPair>() {
                @Override
                public int compare(TrackPair o1, TrackPair o2) {
                    return o1.trackScore - o2.trackScore;
                }
            });
        }
    }

    public static <E> void add(int keep, PriorityQueue<E> priorityQueue, E element) {

        priorityQueue.add(element);

        if (keep < priorityQueue.size()) {
            priorityQueue.poll();
        }
    }

}
