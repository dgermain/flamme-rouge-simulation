package gui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by davidgermain on 2017-06-26.
 */
public class BoardVisualizer extends JPanel {

    private static final long serialVersionUID = 1L;
    //static private final int WIDTH = 1200;
    //static private final int HEIGHT = 800;

    GameBoard currentBoard = null;


    //private Font font = new Font("Arial", Font.BOLD, 18);
    private Font font = new Font("Arial", Font.BOLD, 24);
    FontMetrics metrics;

    static final JFrame f = new JFrame();

    public static void main(String[] args) {
        BoardVisualizer p = new BoardVisualizer(new FlammeRougeBoard());

        /// Code to test some behavior

        p.initialize();
        //initializeVisualizer(p);

        //saveImage("/Users/davidgermain/Documents/BoardgameAnalysis/FlammeRouge/TimelapseFrames/EmptyBoard.jpg");
    }

    public BoardVisualizer(GameBoard board) {
        setGameBoard(board);
        setPreferredSize(new Dimension(250, 250));
        initialize();
    }

    public void initialize(){
        initializeVisualizer(this);

        initializeBoard();
    }

    static public void saveImage(String fileName) {
        Container content = f.getContentPane();
        BufferedImage img = new BufferedImage(content.getWidth(), content.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = img.createGraphics();
        content.printAll(g2d);
        g2d.dispose();

        try {
            System.out.println("Saving image: " + fileName);
            ImageIO.write(img, "png", new File(fileName));
            System.out.println("Success: " + fileName);
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("Failure: " + fileName);
        }
    }

    private void changeToNextTrack(){
        String nextTrack = ((FlammeRougeBoard)currentBoard).getNextTrack();
        if(nextTrack != null){
            ((FlammeRougeBoard)currentBoard).setTrack(nextTrack);
        }
        refresh();
        repaint();
    }

    private void initializeVisualizer(BoardVisualizer p) {
        p.setPreferredSize(new Dimension(currentBoard.getWidth(), currentBoard.getHeight()));

        f.setContentPane(p);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);

        f.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {
                    String testPath = "/Users/davidgermain/Documents/BoardGameAnalysis-Blog/workInProgress/settlementPlacementAnim/test.png";
                    saveImage(testPath);
                }
            }
        });

        f.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                if(e.getKeyChar()=='m'){
                    System.out.println("Key Pressed !!! NNNN");
                    changeToNextTrack();



                    //currentBoard.
                    //currentBoard.initializeBoard();
                }
            }
        });
    }

    FontMetrics numberFontMetrics;

    @Override
    public void paintComponent(Graphics g) {
        BufferedImage img = new BufferedImage(currentBoard.getWidth(), currentBoard.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = img.createGraphics();

        g2d.setStroke(new BasicStroke(4.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
        g2d.setFont(font);
        metrics = g.getFontMetrics();

        Font f = new Font("Futura", Font.BOLD, 24);
        numberFontMetrics = g.getFontMetrics(f);

        currentBoard.drawGameBoard(g2d);

        Graphics2D userVisible = (Graphics2D) g;
        userVisible.drawImage(img,0,0,null);

        g2d.dispose();
    }


    public void refresh(){
        Container content = f.getContentPane();
        BufferedImage img = new BufferedImage(content.getWidth(), content.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = img.createGraphics();
        //super.paintComponent(g2d);
        //paintComponent(g2d);
        content.printAll(g2d);
        g2d.dispose();
    }


    public void setGameBoard(GameBoard board){
        currentBoard = board;
    }

    private void initializeBoard(){
        currentBoard.initializeBoard();
    }



}
