package gui;

//import sun.jvm.hotspot.code.ConstantOopReadValue;
import tools.*;
import trackparts.RoadType;
import trackparts.TrackInfo;
import trackparts.TrackShape;

import java.awt.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.Stack;

import static tools.Coordinates.*;

/**
 * Created by davidgermain on 2018-01-17.
 */
public class FlammeRougeBoard implements GameBoard {
    private static final long serialVersionUID = 1L;
    static private int WIDTH = 1200;
    static private int HEIGHT = 800;

    private Font font = new Font("Arial", Font.BOLD, 24);
    FontMetrics numberFontMetrics;

    private final int baseTrackWidth = 64;
    private final int baseStraightLength = 192;
    private final int baseInnerRadius = 15;

    double factorLength = 0.45;

    private double trackWidth = 64;
    private double sideShiftLength = (trackWidth / 2.0);
    private double straigthLength = 192;
    private double innerRadius = 15;

    double currentDirection = 0.0;
    private int currentAngleStep = 0;

    CoordinatesFloat upperLeftBoundingBox = null;
    CoordinatesFloat lowerRightBoundingBox = null;

    Color roadColor = new Color(0xa0a0a0);// new Color(0xD6D6D6);
    Color upColor = new Color(0xFF0000);// new Color(0xD6D6D6);
    Color downColor = new Color(0x0000FF);// new Color(0xD6D6D6);
    Color startLineColor = new Color(0xFFFF00);// new Color(0xD6D6D6);
    Color finishColor = new Color(0xE88B86);// new Color(0xD6D6D6);

    RoadType lastTrackType = RoadType.PRESTART;

    //LineCrossingChecker trackChecker;
    //CoordinatesFloat currentPos;
    //Stack<TrackShape> shapeStack = new Stack<>();
    //Stack<CoordinatesFloat> positionStack = new Stack<>();
    TrackBuilderHelper builderHelper = new TrackBuilderHelper(factorLength);

    public int getWidth(){
        return WIDTH;
    }
    public int getHeight(){
        return HEIGHT;
    }

    public FlammeRougeBoard(){

    }

    public void initializeBoard(){

    }

    public void drawGameBoard(Graphics2D g2d) {

        Color backgroundColor = Color.white;
        Point origin = new Point(WIDTH / 2, HEIGHT / 2);

        resizeTrackDrawing(factorLength);

        //Draw a Big White  Circle for background test
        DrawingTools.drawCircle(g2d, origin, 880, true, true, backgroundColor.getRGB(), 0);

        CoordinatesFloat testPt = new CoordinatesFloat(600, 400.0);
        CoordinatesFloat startPt = drawRaceTrack(g2d, testPt);

        //Set by external code to check line for other code.
//        if(nextPtRight != null){
//            //Draw all shape based on pre-calculated coordinates
//            drawShapes(g2d);
//        }

        //If Race Track is not correctly center, redraw racetrack....
//        if (startPt != null) {
//            DrawingTools.drawCircle(g2d, origin, 880, true, true, backgroundColor.getRGB(), 0);
//            System.out.println("StartPt: " + startPt.toString());
//            drawRaceTrack(g2d, startPt);
//        }

        drawElevationChart(g2d);

        if(startTimelapse){
            //Save board to file...

        }
    }

    /***
     * Resize used length for actual drawing on screen
     * @param factorLength Multiplier of position size to resize screen drawing
     */
    private void resizeTrackDrawing(double factorLength) {
        trackWidth = ((double)baseTrackWidth * factorLength);
        sideShiftLength = trackWidth / 2.0;
        straigthLength = ((double)baseStraightLength * factorLength);
        innerRadius =((double)baseInnerRadius * factorLength);
    }


    private CoordinatesFloat drawRaceTrack(Graphics2D g2d, CoordinatesFloat drawingStartPos) {

        g2d.setStroke(new BasicStroke(4.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
        g2d.setFont(font);

        Font f = new Font("Futura", Font.BOLD, 24);
        numberFontMetrics = g2d.getFontMetrics(f);

        CoordinatesFloat startingPoint = new CoordinatesFloat((double)(WIDTH / 2), (double)(HEIGHT/ 2));

        if(drawingStartPos != null){
            startingPoint = startingPoint.add(startingPoint.subtract(drawingStartPos));
        }

        upperLeftBoundingBox = startingPoint.copy();
        lowerRightBoundingBox = startingPoint.copy();

        System.out.println("Drawing RaceTrack: " + currentTrackString);
        //CoordinatesFloat currentPosition = new CoordinatesFloat(startingPoint.x,startingPoint.y);
        CoordinatesFloat currentPos = new CoordinatesFloat(startingPoint.x,startingPoint.y);

        currentDirection = 0.0;
        currentAngleStep = 0;

        CoordinatesFloat shiftVector = getShiftVectorFloat(sideShiftLength, builderHelper.stepToRadian(currentAngleStep));

        CoordinatesFloat rightStart = currentPos.add(shiftVector);
        CoordinatesFloat leftStart  = currentPos.subtract(shiftVector);

        LineCrossingChecker lineChecker = new LineCrossingChecker(rightStart, leftStart);

        for(char raceElement: currentTrackString.toCharArray()){

            String raceElementStr = "" + raceElement;
            TrackInfo track = availableTracks.get(raceElementStr);

            //shiftVector = getShiftVectorFloat(sideShiftLength, currentDirection);
            shiftVector = getShiftVectorFloat(sideShiftLength, builderHelper.stepToRadian(currentAngleStep));

            rightStart = currentPos.add(shiftVector);
            leftStart  = currentPos.subtract(shiftVector);

            TrackShape shape = track.getTrackShape();

            if(track.getTrackShape() == TrackShape.STRAIGHT){

                CoordinatesFloat[] nextPointsLeft  = builderHelper.nextPtLeftFast[shape.ordinal()][currentAngleStep];
                CoordinatesFloat[] nextPointsRight = builderHelper.nextPtRightFast[shape.ordinal()][currentAngleStep];

                CoordinatesFloat nextStartPos = currentPos.add(builderHelper.shapeEndPointFast[shape.ordinal()][currentAngleStep]);


                CoordinatesFloat leftEnd  = currentPos.add(nextPointsLeft[0]);
                CoordinatesFloat rightEnd = currentPos.add(nextPointsRight[0]);

                //CoordinatesFloat trackVector = getLengthVectorFloat(straigthLength, builderHelper.stepToRadian(currentAngleStep));

                g2d.setColor(new Color(roadColor.getRGB()));

                drawLine(g2d, currentPos, nextStartPos);
                drawLine(g2d, leftStart, leftEnd);
                drawLine(g2d, rightStart, rightEnd);

                //Draw Start Line bar
                drawLine(g2d,leftStart, rightStart);


                //Draw spaces line for raceTrack
                for(int j=0;j< track.getTrackLength();++j){
                    int inLength = (int)((j) * (straigthLength / track.getTrackLength()));

                    Color trackColor = getTrackColor(track.getTrackSegmentType(j), lastTrackType);
                    g2d.setColor(trackColor);

                    CoordinatesFloat tempPoint = currentPos.add( new CoordinatesFloat((inLength * Math.cos(currentDirection)), (inLength * Math.sin(currentDirection))));

                    drawLine(g2d,tempPoint.subtract(shiftVector), tempPoint.add(shiftVector));

                    //For further references....
                    lastTrackType = track.getTrackSegmentType(j);
                }

                //Check for line Validity at end point.
                lineChecker.addEndPoint(rightEnd, leftEnd);
                drawLine(g2d,rightEnd, leftEnd);

                //Update Position And Angle for next piece
                currentPos = nextStartPos;
                currentAngleStep = (16 + currentAngleStep + builderHelper.angleStepChangeDir.get(shape)) & 15;

            }else if(track.getTrackShape() == TrackShape.LEFT_45 || track.getTrackShape() == TrackShape.RIGHT_45){
                currentPos = process45Turn(g2d, track, currentPos, track.getTrackShape(), lineChecker);
            }

            else if(track.getTrackShape() == TrackShape.LEFT_90 || track.getTrackShape() == TrackShape.RIGHT_90){
                currentPos = process90Turn(g2d, track, currentPos, track.getTrackShape(), lineChecker);

            }

            evaluateBoundingBox(currentPos);


            if(!lineChecker.isValid()){
                System.out.println("Line Segment Not Valid at: " + track.getId());
            }
        }

        //Line Checker drawing...
        lineChecker.drawLines(g2d,drawingStartPos);


        if(upperLeftBoundingBox.x < 0 || lowerRightBoundingBox.x > WIDTH || upperLeftBoundingBox.y <0 || upperLeftBoundingBox.y > HEIGHT){
            System.out.println("Reprinting Racetrack. Out Of Bound " );
            return new CoordinatesFloat((int)(upperLeftBoundingBox.x + lowerRightBoundingBox.x)/2,(int)(upperLeftBoundingBox.y + lowerRightBoundingBox.y)/2) ;
        }

        writeValidityText(g2d, lineChecker.isValid());
        g2d.setColor(roadColor);

        return null;
    }

    private void writeValidityText(Graphics2D g2d, boolean  isValid) {
        FontMetrics currentFont = numberFontMetrics; //metrics;
        g2d.setFont(currentFont.getFont());

        String validityText = "Track Valid - ";
        g2d.setColor(new Color(0x1FFF1F));
        if(!isValid){
            g2d.setColor(new Color(0xFF0000));
            validityText = "Track not Valid !!! - ";
        }
        int w = currentFont.stringWidth(validityText);
        int h = currentFont.getHeight();

        //Write number in square!!!
        int textX = 20;
        int textY = 20;

        g2d.drawString(validityText, textX , textY + h/2 );

        g2d.setColor(new Color(0x080101));
        g2d.drawString(currentTrackString, textX + w, textY + h/2 );
    }

    private Color getTrackColor(RoadType trackSegmentType, RoadType lastRoadType) {
        Color trackColor = roadColor;

        if(trackSegmentType == RoadType.UP){
            trackColor = upColor;
        }else if(trackSegmentType == RoadType.DOWN) {
            trackColor = downColor;
        }else if(trackSegmentType == RoadType.PRESTART) {
            trackColor = startLineColor;
        }else if(trackSegmentType == RoadType.FINISH) {
            trackColor = finishColor;
        }else if(trackSegmentType == RoadType.FLAT){

            if(lastRoadType == RoadType.DOWN){
                trackColor = downColor;
            }else if(lastRoadType == RoadType.UP){
                trackColor = upColor;
            }else{
                trackColor = roadColor;
            }
        }

        return trackColor;
    }

    private CoordinatesFloat process90Turn(Graphics2D g2d, TrackInfo track, CoordinatesFloat currentPosition, TrackShape trackShape, LineCrossingChecker lineChecker) {

        CoordinatesFloat startPos = currentPosition.copy();
        TrackShape shape = track.getTrackShape();
        CoordinatesFloat[] nextPointsLeft  = builderHelper.nextPtLeftFast[shape.ordinal()][currentAngleStep];
        CoordinatesFloat[] nextPointsRight = builderHelper.nextPtRightFast[shape.ordinal()][currentAngleStep];

        CoordinatesFloat nextStartPos = startPos.add(builderHelper.shapeEndPointFast[shape.ordinal()][currentAngleStep]);

        int trackSide = (int)( trackWidth);
        CoordinatesFloat shiftVector = getShiftVectorFloat(sideShiftLength, currentDirection);

        CoordinatesFloat rightStart = currentPosition.add(shiftVector);
        CoordinatesFloat leftStart  = currentPosition.subtract(shiftVector);

        CoordinatesFloat shiftCenterVector = getShiftVectorFloat( innerRadius+sideShiftLength, currentDirection);
        CoordinatesFloat rightCenter = currentPosition.add(shiftCenterVector);
        CoordinatesFloat leftCenter  = currentPosition.subtract(shiftCenterVector);

        CoordinatesFloat exteriorStart = null;
        CoordinatesFloat interiorStart = null;
        int turnAngle = 0;
        CoordinatesFloat circleCenter;
        int midTurn =0;

        boolean inverseMid = false;
        if(trackShape.equals(TrackShape.LEFT_90)){
            exteriorStart = rightStart.copy();
            interiorStart = leftStart.copy();
            circleCenter = leftCenter;
            turnAngle = 90;
            midTurn = 45;
            //Important for drawing fuction...
            inverseMid = true;

        }else if(trackShape.equals(TrackShape.RIGHT_90)){
            exteriorStart = leftStart.copy();
            interiorStart = rightStart.copy();
            circleCenter = rightCenter;
            turnAngle = -90;
            midTurn = -45 + 180;

        }else {
            System.out.println("#########   ERROR, should only be called by a 45 degree turn tile... #######");
            return null;
        }

        int exteriorRadius = (int)((innerRadius+trackWidth) );
        int middleRadius = (int)((innerRadius+(trackWidth/2.0)));
        int interiorRadius = (int)(innerRadius );

        CoordinatesFloat midInterior = getCircleAdjust(circleCenter, interiorRadius, currentDirection, midTurn);
        CoordinatesFloat midMiddle   = getCircleAdjust(circleCenter, middleRadius, currentDirection, midTurn);
        CoordinatesFloat midExterior = getCircleAdjust(circleCenter, exteriorRadius, currentDirection, midTurn);

        Color trackColor = getTrackColor(track.getTrackSegmentType(1),null);
        g2d.setColor(trackColor);

        drawLine(g2d,midInterior,midExterior);
//        if(inverseMid){
//            lineChecker.addEndPoint(midExterior,  midInterior);
//        }else {
//            lineChecker.addEndPoint(midInterior, midExterior);
//        }


        //For further references....

        //Draw both track length of second square color
        drawCurb(g2d, currentPosition, middleRadius, currentDirection, turnAngle);
        drawCurb(g2d, exteriorStart, exteriorRadius, currentDirection, turnAngle);
        drawCurb(g2d, interiorStart, interiorRadius, currentDirection, turnAngle);

        //Consider previous track type for starting line.
        trackColor = getTrackColor(track.getTrackSegmentType(0),lastTrackType);
        g2d.setColor(trackColor);
        drawLine(g2d,interiorStart,exteriorStart);

        //The last type is the second square (and no curb has 2 color)
        lastTrackType = track.getTrackSegmentType(1);

        //Only consider current track type for side color.
        trackColor = getTrackColor(track.getTrackSegmentType(0),null);
        g2d.setColor(trackColor);

        //Half Turn
        currentDirection += Math.toRadians(-turnAngle/2.0);

        double straightLength = Math.sqrt(Math.pow(innerRadius+(trackWidth/2.0),2) * 2);// * factorLength;


        CoordinatesFloat trackVector = getLengthVectorFloat(straightLength,currentDirection);

        CoordinatesFloat endPosition = currentPosition.add(trackVector);

        Color backgroundColor = new Color(0x000000);// new Color(0xD6D6D6);

        //g2d.setColor(new Color(backgroundColor.getRGB()));
        g2d.setColor(trackColor);
        drawLine(g2d,interiorStart,exteriorStart);

        currentDirection += Math.toRadians(-turnAngle/2.0);
        currentAngleStep = (16 + currentAngleStep + builderHelper.angleStepChangeDir.get(track.getTrackShape())) & 15;


        shiftVector = getShiftVectorFloat((trackSide/2.0), currentDirection);

        //lineChecker.addEndPoint(endPosition.add(shiftVector),endPosition.subtract(shiftVector));

        lineChecker.addEndPoint(startPos.add(nextPointsRight[0]), startPos.add(nextPointsLeft[0]));
        lineChecker.addEndPoint(startPos.add(nextPointsRight[1]), startPos.add(nextPointsLeft[1]));


        drawLine(g2d,endPosition.add(shiftVector),endPosition.subtract(shiftVector));

        return nextStartPos;
    }

    private CoordinatesFloat getCurbStartPt(double radius, double startingAngle){
        double circleCenterX = radius;
        double circleCenterY = radius;

        double startArcPosition =  startingAngle + (Math.PI);

        double startXPos =  (circleCenterX + radius * Math.sin(startArcPosition));
        double startYPos =  (circleCenterY + radius * Math.cos(startArcPosition));

        return new CoordinatesFloat(startXPos,startYPos);
    }

    private void drawCurb(Graphics2D g2d, CoordinatesFloat startPt, int radius, double rawDirection, int angle){

        double direction = 0;
        if(angle > 0){
            direction = -rawDirection + Math.PI;
        }else{
            direction = -rawDirection;
        }

        //Determine starting pixel for Startint Point
        CoordinatesFloat curbShift = getCurbStartPt((int)radius, direction);

        Coordinates curbStart = (startPt.subtract(curbShift)).toCoordinates();

        int degreeToUse = (int)(Math.toDegrees(direction + Math.PI/2));
        if(degreeToUse > 360)
            degreeToUse -= 360;
        if(degreeToUse < 0)
            degreeToUse += 360;


        g2d.drawArc(curbStart.x, curbStart.y, radius * 2, radius * 2 , degreeToUse, angle);
    }

    //circleCenter, interiorRadius, currentDirection, turnAngle/2.0
    private CoordinatesFloat getCircleAdjust(CoordinatesFloat circleCenter, int radius, double direction, double angle) {

        CoordinatesFloat deltaCoord = getCircleAdjust(radius, direction, angle);

        CoordinatesFloat answer = new CoordinatesFloat(circleCenter.x + deltaCoord.x, circleCenter.y + deltaCoord.y);
        return answer;
    }

    private CoordinatesFloat getCircleAdjust(double radius, double direction, double angle) {

        double radAngle = Math.toRadians(angle);
        CoordinatesFloat circleAdjust = new CoordinatesFloat(Math.sin(radAngle) * radius, Math.cos(radAngle) * radius);

        double cosAngle = Math.cos(direction );//+ Math.toRadians(90));
        double sinAngle = Math.sin(direction );//+ Math.toRadians(90));

        double xRot = cosAngle * circleAdjust.x - sinAngle * circleAdjust.y;
        double yRot = sinAngle * circleAdjust.x + cosAngle * circleAdjust.y;

        CoordinatesFloat adjustment = new CoordinatesFloat(xRot,yRot);

        return adjustment;
    }

    private double baseRadius45 = 10.0;
    private double baseTrackLength45 = 15.0;

    private CoordinatesFloat process45Turn(Graphics2D g2d, TrackInfo track, CoordinatesFloat currentPosition, TrackShape trackShape, LineCrossingChecker lineChecker) {

        CoordinatesFloat startPos = currentPosition.copy();
        double radius45 = factorLength * baseRadius45;
        double trackLength45 = factorLength * baseTrackLength45;

        CoordinatesFloat shiftVector = getShiftVectorFloat(sideShiftLength, builderHelper.stepToRadian(currentAngleStep));

        TrackShape shape = track.getTrackShape();
        CoordinatesFloat[] nextPointsLeft  = builderHelper.nextPtLeftFast[shape.ordinal()][currentAngleStep];
        CoordinatesFloat[] nextPointsRight = builderHelper.nextPtRightFast[shape.ordinal()][currentAngleStep];

        CoordinatesFloat nextStartPos = currentPosition.add(builderHelper.shapeEndPointFast[shape.ordinal()][currentAngleStep]);


        //CoordinatesFloat rightStart;// = currentPosition.add(shiftVector);
        //CoordinatesFloat leftStart;//  = currentPosition.subtract(shiftVector);

        CoordinatesFloat exteriorStart;// = null;
        CoordinatesFloat interiorStart;// = null;
        int turnAngle = 0;
        double midTurn = 0;

        CoordinatesFloat shiftCenterVector = getShiftVectorFloat((radius45+(trackWidth/2.0)), builderHelper.stepToRadian(currentAngleStep));
        //CoordinatesFloat rightCenter = currentPosition.add(shiftCenterVector);
        //CoordinatesFloat leftCenter  = currentPosition.subtract(shiftCenterVector);

        CoordinatesFloat circleCenter;

        if(trackShape.equals(TrackShape.LEFT_45)){
            exteriorStart = currentPosition.add(shiftVector);;//rightStart.copy();
            interiorStart = currentPosition.subtract(shiftVector);//leftStart.copy();
            circleCenter = currentPosition.subtract(shiftCenterVector);//leftCenter.copy();
            turnAngle = 45;
            midTurn = (turnAngle+3) /2.0;


        }else if(trackShape.equals(TrackShape.RIGHT_45)){
            exteriorStart = currentPosition.subtract(shiftVector);//leftStart.copy();
            interiorStart = currentPosition.add(shiftVector);//rightStart.copy();
            circleCenter = currentPosition.add(shiftCenterVector);//rightCenter.copy();
            turnAngle = -45;
            midTurn = 180 + ((turnAngle-3) /2.0);
        }else {
            System.out.println("Error, should only be called by a 45 degree turn tile...");
            return null;
        }

        Color trackColor = getTrackColor(track.getTrackSegmentType(1),null);
        g2d.setColor(trackColor);

        // Draw 1.5 cm straight //
        CoordinatesFloat trackVectorToCurbStart = new CoordinatesFloat(
                trackLength45 * Math.cos(currentDirection),
                trackLength45 * Math.sin(currentDirection) );

        drawLine(g2d, currentPosition, currentPosition.add(trackVectorToCurbStart));
        drawLine(g2d, interiorStart, interiorStart.add(trackVectorToCurbStart));
        drawLine(g2d, exteriorStart, exteriorStart.add(trackVectorToCurbStart));

        //Start of Curb
        currentPosition = currentPosition.add(trackVectorToCurbStart);
        exteriorStart = exteriorStart.add(trackVectorToCurbStart);
        interiorStart = interiorStart.add(trackVectorToCurbStart);

        //Draw Bend
        int exteriorRadius = (int)(75.0 * factorLength);
        int middleRadius   = (int)(radius45 +(trackWidth/2.0));
        int interiorRadius = (int)(radius45);

        circleCenter = circleCenter.add(trackVectorToCurbStart);

        CoordinatesFloat midInterior = getCircleAdjust(circleCenter, interiorRadius, currentDirection, midTurn);
        CoordinatesFloat midMiddle   = getCircleAdjust(circleCenter, middleRadius, currentDirection, midTurn);
        CoordinatesFloat midExterior = getCircleAdjust(circleCenter, exteriorRadius, currentDirection, midTurn);

        CoordinatesFloat rightTest = currentPosition.add(nextPointsRight[0]);
        CoordinatesFloat leftTest = currentPosition.add(nextPointsLeft[0]);

        drawLine(g2d, midInterior, midExterior);
        g2d.setColor(trackColor);

        drawCurb(g2d, currentPosition, middleRadius, currentDirection,turnAngle);
        drawCurb(g2d, exteriorStart, exteriorRadius, currentDirection,turnAngle);
        drawCurb(g2d, interiorStart, interiorRadius, currentDirection,turnAngle);

        //Middle Point
        int lengthToMidPoint = 20;
        CoordinatesFloat trackVectorToCurbMidpoint = getLengthVectorFloat(lengthToMidPoint, factorLength, currentDirection);

        //Set at Curb Midpoint
        currentPosition = currentPosition.add(trackVectorToCurbMidpoint);

        //TODO: Should add right,left point to crossing line info
        //////


        //Turning Left, half of 45 degrees....
        currentDirection += Math.toRadians(-turnAngle);
        currentAngleStep = (16 + currentAngleStep + builderHelper.angleStepChangeDir.get(track.getTrackShape())) & 15;

        CoordinatesFloat trackVector = getLengthVectorFloat(lengthToMidPoint, factorLength, currentDirection);

        //Skip to start of Line To Draw.
        currentPosition = currentPosition.add(trackVector);

        shiftVector = getShiftVectorFloat((trackWidth/2.0), currentDirection);


        CoordinatesFloat rightStart = currentPosition.add(shiftVector);
        CoordinatesFloat leftStart  = currentPosition.subtract(shiftVector);

        trackVector = getLengthVectorFloat(lengthToMidPoint, factorLength, currentDirection);

        //Draw last length of track
        drawLine(g2d, currentPosition, currentPosition.add(trackVector));
        drawLine(g2d, rightStart, rightStart.add(trackVector));
        drawLine(g2d, leftStart, leftStart.add(trackVector));

        //Check for line Validity at end point.
        //lineChecker.addEndPoint(rightStart.add(trackVector), leftStart.add(trackVector));
        lineChecker.addEndPoint(startPos.add(nextPointsRight[0]), startPos.add(nextPointsLeft[0]));
        lineChecker.addEndPoint(startPos.add(nextPointsRight[1]), startPos.add(nextPointsLeft[1]));

        //nextPointsRight

        drawLine(g2d,rightStart.add(trackVector), leftStart.add(trackVector));

        //Return last current Position;
        return currentPosition.add(trackVector);
    }

    private void drawElevationChart(Graphics2D g2d) {
        g2d.setStroke(new BasicStroke(4.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
        g2d.setFont(font);

        Font f = new Font("Futura", Font.BOLD, 24);
        numberFontMetrics = g2d.getFontMetrics(f);

        Coordinates chartSize = new Coordinates(400,200);
        Coordinates elevationChartLeftCorner = new Coordinates(WIDTH -chartSize.x, HEIGHT - chartSize.y);

        Color borderColor = Color.black;
        g2d.setColor(new Color(borderColor.getRGB()));

        drawLine(g2d, elevationChartLeftCorner, new Coordinates(elevationChartLeftCorner.x, HEIGHT));
        drawLine(g2d, elevationChartLeftCorner, new Coordinates(WIDTH, elevationChartLeftCorner.y));

        Coordinates elevationGridTopCorner = elevationChartLeftCorner.add(new Coordinates(15,15));

        int minUnit = 5;
        int gridSizeX = 90/minUnit;
        int gridSizeY = 40/minUnit;
        int squareSize = (chartSize.x -30)/90*minUnit;

        drawElevationGrid(g2d,elevationGridTopCorner, gridSizeX, gridSizeY, squareSize);

        Coordinates lineStartPt = elevationGridTopCorner.add(new Coordinates(0, gridSizeY/2*squareSize));
        drawElevationLine(g2d,lineStartPt, squareSize/minUnit );
    }



    private void drawElevationLine(Graphics2D g2d, Coordinates lineStartPt, int squareSize) {

        Coordinates currentPt = lineStartPt.copy();

        Coordinates slopeUpPoint = new Coordinates(0, -squareSize);
        for (int i = 0; i < currentTrackString.length(); ++i) {
            String raceElement = currentTrackString.substring(i, i + 1);

            TrackInfo track = availableTracks.get(raceElement);

            Color lineColor = new Color(0x000000);// new Color(0xD6D6D6);


            for(int j = 0; j< track.getTrackLength();j++){

                //AdvancePt;
                Coordinates endPt = currentPt.add(new Coordinates(squareSize,0));

                switch (track.getTrackSegmentType(j)){
                    case PRESTART:
                        lineColor = startLineColor;
                        break;
                    case FLAT:
                        lineColor = Color.black;
                        break;
                    case UP:
                        endPt = endPt.add(slopeUpPoint);
                        lineColor = Color.red;
                        break;
                    case DOWN:
                        endPt = endPt.subtract(slopeUpPoint);
                        lineColor = Color.blue;
                        break;
                    case FINISH:
                        lineColor = Color.pink;
                        break;
                }

                g2d.setColor(new Color(lineColor.getRGB()));
                drawLine(g2d,currentPt,endPt);
                currentPt = endPt;
            }
        }
    }

    private void drawElevationGrid(Graphics2D g2d, Coordinates elevationGridTopCorner, int gridSizeX, int gridSizeY, int squareSize) {

        Color borderColor = new Color(0xE0E0E0);// new Color(0xD6D6D6);
        g2d.setColor(new Color(borderColor.getRGB()));
        for(int i=0;i<=gridSizeX;++i){
            int topX = elevationGridTopCorner.x + i*squareSize;
            int bottomY = elevationGridTopCorner.y + gridSizeY* squareSize;
            g2d.drawLine(topX, elevationGridTopCorner.y, topX, bottomY);
        }

        for(int i=0;i<=gridSizeY;++i){
            int endX = elevationGridTopCorner.x + gridSizeX*squareSize;
            int startY = elevationGridTopCorner.y + i* squareSize;
            g2d.drawLine(elevationGridTopCorner.x, startY, endX, startY);
        }
    }

    private void drawLine(Graphics2D g2d, CoordinatesFloat start, CoordinatesFloat end) {

        Coordinates startPt = start.toCoordinates();
        Coordinates endPt = end.toCoordinates();
        g2d.drawLine(startPt.x, startPt.y, endPt.x, endPt.y);
    }

    private void drawLine(Graphics2D g2d, Coordinates currentPosition, Coordinates endPosition) {
        g2d.drawLine(currentPosition.x, currentPosition.y, endPosition.x, endPosition.y);
    }

    private void drawSquare(Graphics2D g2d, int startX, int startY, int sideLength) {
        g2d.drawLine(startX, startY, startX + sideLength, startY);
        g2d.drawLine(startX, startY, startX, startY + sideLength);
        g2d.drawLine(startX + sideLength, startY, startX + sideLength, startY + sideLength);
        g2d.drawLine(startX, startY + sideLength, startX + sideLength, startY + sideLength);
    }

    private int getCurbStartDirection(double currentDirection) {
        if(Math.abs(currentDirection - 0) < 0.1){
            return 0;
        }else if(Math.abs(currentDirection - (Math.PI*2)/8) < 0.1 ){ // 45

        }
        return 0;
    }

    String currentTrackString = "";
    Map<String, TrackInfo> availableTracks;

    public void setTrack(String raceTrack, Map<String, TrackInfo> availableTracks) {
        currentTrackString = raceTrack;
        this.availableTracks = availableTracks;
    }

    public void setTrack(String raceTrack) {
        currentTrackString = raceTrack;
    }

    Map<ShapeKey, Coordinates[]> nextPtRight = null;
    Map<ShapeKey, Coordinates[]> nextPtLeft = null;
    public void setShapes(Map<ShapeKey, Coordinates[]> nextPtRight_in, Map<ShapeKey, Coordinates[]> nextPtLeft_in) {
        nextPtRight = nextPtRight_in;
        nextPtLeft = nextPtLeft_in;
    }

    ArrayList<String> nextTracks = new ArrayList<>();
    public void setNextTracks(ArrayList<String> listOfTracks) {
        nextTracks = listOfTracks;
    }
    public String getNextTrack(){
        if(nextTracks.size() > 0){
            String track =  nextTracks.get(nextTracks.size()-1);
            nextTracks.remove(nextTracks.size()-1);
            return track;
        }
        return null;
    }



    private void drawShapes(Graphics2D g2d) {
        for(int i=0;i<16;i++){
            ShapeKey key = new ShapeKey(TrackShape.RIGHT_45,i);

            int startX = 100 * i;
            int startY = 200;
            Coordinates startPt = new Coordinates(startX,startY);

            Coordinates[] right = nextPtRight.get(key);
            Coordinates[] left = nextPtLeft.get(key);

            Coordinates shiftVector = getShiftVector(64/2,Math.toRadians(22.5*i));

            Coordinates startRight =  startPt.add(shiftVector);
            Coordinates startLeft =  startPt.subtract(shiftVector);

            Color backgroundColor = new Color(0x000000);// new Color(0xD6D6D6);

            g2d.setColor(new Color(backgroundColor.getRGB()));

            drawLine(g2d,startRight,startLeft);

            Coordinates lastLeft = startLeft;
            Coordinates lastRight = startRight;
            for(int j=0; j< right.length;++j) {
                Coordinates endPtRight = startPt.add(right[j]);
                Coordinates endPtLeft = startPt.add(left[j]);
                drawLine(g2d, lastRight, endPtRight);
                drawLine(g2d, endPtLeft, lastLeft);
                drawLine(g2d, endPtLeft, endPtRight);

                lastRight =  endPtRight;
                lastLeft = endPtLeft;
            }

            //Color redColor = new Color(0xFF0000);// new Color(0xD6D6D6);

            g2d.setColor(new Color(backgroundColor.getRGB()));

            g2d.drawOval(startPt.x,startPt.y,5,5);

        }

        for(int i=0;i<16;i++){
            ShapeKey key = new ShapeKey(TrackShape.LEFT_45,i);

            int startX = 100 * i;
            int startY = 500;
            Coordinates startPt = new Coordinates(startX,startY);

            Coordinates[] right = nextPtRight.get(key);
            Coordinates[] left = nextPtLeft.get(key);

            Coordinates shiftVector = getShiftVector(64/2,Math.toRadians(22.5*i));

            Coordinates startRight =  startPt.add(shiftVector);
            Coordinates startLeft =  startPt.subtract(shiftVector);

            Color backgroundColor = new Color(0x000000);// new Color(0xD6D6D6);

            g2d.setColor(new Color(backgroundColor.getRGB()));

            drawLine(g2d,startRight,startLeft);

            for(int j=0; j< right.length;++j) {
                Coordinates endPtRight = startPt.add(right[j]);
                Coordinates endPtLeft = startPt.add(left[j]);
                drawLine(g2d, startRight, endPtRight);
                drawLine(g2d, endPtLeft, startLeft);
                drawLine(g2d, endPtLeft, endPtRight);
            }

            Color redColor = new Color(0xFF0000);// new Color(0xD6D6D6);

            g2d.setColor(new Color(backgroundColor.getRGB()));

            g2d.drawOval(startPt.x,startPt.y,5,5);
        }
    }


    private void evaluateBoundingBox(CoordinatesFloat point){
        if(point.x < upperLeftBoundingBox.x){
            upperLeftBoundingBox.x = point.x;
        }

        if(point.x > lowerRightBoundingBox.x){
            lowerRightBoundingBox.x = point.x;
        }

        if(point.y < upperLeftBoundingBox.y){
            upperLeftBoundingBox.y = point.y;
        }

        if(point.y > lowerRightBoundingBox.y){
            lowerRightBoundingBox.y = point.y;
        }
    }



//    private void drawBuiltTrack(Graphics2D g2d) {
//        ArrayList<CoordinatesFloat> coords = trackChecker.getAllCoordinates();
//
//        ArrayList<CoordinatesFloat> rightCoord = trackChecker.getAllCoordinates();
//        ArrayList<CoordinatesFloat> leftCoord = trackChecker.getAllCoordinates();
//        for(int i=0 ;i< coords.size()/2;++i ){
//            rightCoord.add( resize(factorLength,coords.get(i)));
//            leftCoord.add(resize(factorLength,coords.get(i + coords.size()/2)));
//        }
//
//        for(int i=0 ;i < rightCoord.size();++i ){
//            drawLine(g2d, rightCoord.get(i), leftCoord.get(i));
//            if(i< rightCoord.size()-1){
//                drawLine(g2d, resize(factorLength,rightCoord.get(i)), resize(factorLength,rightCoord.get(i + 1)));
//            }
//        }
//
//    }

    private CoordinatesFloat resize(double factorLength, CoordinatesFloat coord) {
        return new CoordinatesFloat(coord.x*factorLength,coord.y*factorLength);
    }

    boolean startTimelapse = false;
    String timelapseFolder;
    public void startTimelapse(String folderOut) {

        timelapseFolder = folderOut;
        startTimelapse = true;


    }

//    private void buildRaceTrack(CoordinatesFloat startPos) {
//        currentDirection = 0.0;
//        currentPos = startPos;
//
//        int trackSide = (int) (factorLength * trackWidth);
//
//        CoordinatesFloat shiftVector = getShiftVectorFloat((trackWidth / 2.0), currentDirection);
//
//        CoordinatesFloat rightStart = currentPos.add(shiftVector);
//        CoordinatesFloat leftStart = currentPos.subtract(shiftVector);
//
//        //This will keep track of all segments and check if they intersect.
//        trackChecker = new LineCrossingChecker(rightStart, leftStart);
//
//        for (int i = 0; i < currentTrackString.length(); ++i) {
//            String raceElement = currentTrackString.substring(i, i + 1);
//
//            TrackInfo track = availableTracks.get(raceElement);
//
//            System.out.println("Using element: " + track.toString());
//            TrackShape shape = track.getTrackShape();
//
//            //Add element to the built track...
//            CoordinatesFloat[] nextPointsLeft = builderHelper.nextPtLeftFast[shape.ordinal()][currentAngleStep];
//            CoordinatesFloat[] nextPointsRight = builderHelper.nextPtRightFast[shape.ordinal()][currentAngleStep];
//
//            trackChecker.addEndPoint(currentPos.add(nextPointsRight[0]), currentPos.add(nextPointsLeft[0]));
//
//            if (nextPointsLeft.length > 1) {
//                trackChecker.addEndPoint(currentPos.add(nextPointsRight[1]), currentPos.add(nextPointsLeft[1]));
//            }
//
//            currentPos = currentPos.add(builderHelper.shapeEndPointFast[shape.ordinal()][currentAngleStep]);
//            shapeStack.push(shape);
//            positionStack.push(currentPos);
//
//            currentAngleStep = (16 + currentAngleStep + builderHelper.angleStepChangeDir.get(shape)) & 15;
//        }
//
//    }

}
