package gui;

import java.awt.*;

/**
 * Created by davidgermain on 2018-01-17.
 */
public interface GameBoard {
    void initializeBoard();

    void drawGameBoard(Graphics2D g2d);

    int getWidth();
    int getHeight();

    //public GameBoard();
}
