package tools;

import java.util.Stack;

public class PartsToBuild {

        Stack<String> straightParts = new Stack<>();
        Stack<String> left45Parts = new Stack<>();
        Stack<String> left90Parts = new Stack<>();

public PartsToBuild(){
        addToArray(straightParts, "abcdflmnu");
        addToArray(left45Parts,   "tKhSRq");
        addToArray(left90Parts,"JeOiPg");
        }

public String getStraight(){
        return straightParts.pop();
        }

public String changeCase(String letter){
        if(letter.charAt(0) >= 'a' && letter.charAt(0) <='z'){
        return letter.toUpperCase();
        }
        else
        return letter.toLowerCase();
        }

public String getLeft45(boolean isLeft){
        String part = left45Parts.pop();

        if(isLeft)
        return part;
        else
        return changeCase(part);
        }

public String getLeft90(boolean isLeft){
        String part = left90Parts.pop();

        if(isLeft)
        return part;
        else
        return changeCase(part);
        }

public void addToArray(Stack<String> emptyArray, String parts){
        char[] charPart = parts.toCharArray();

        for(int i = charPart.length -1 ; i>=0; --i){
        String t = "" +charPart[i];
        emptyArray.push(t);
        }
        }
        }