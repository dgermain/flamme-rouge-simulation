package tools;

/**
 * Created by davidgermain on 2018-01-23.
 */
public class Coordinates {
    public int x;
    public int y;

    public Coordinates(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Coordinates add(Coordinates shiftPosition) {
        Coordinates newPoint = new Coordinates(x + shiftPosition.x, y + shiftPosition.y);

        return newPoint;
    }

    public Coordinates subtract(Coordinates shiftPosition) {
        Coordinates newPoint = new Coordinates(x - shiftPosition.x, y - shiftPosition.y);

        return newPoint;
    }

    public Coordinates copy() {
        Coordinates newPoint = new Coordinates(x, y);

        return newPoint;
    }


    public static Coordinates getShiftVector(int shiftLength, double currentDirection) {
        return new Coordinates( (int)(shiftLength * Math.cos(currentDirection+(Math.PI/2.0))),
                (int)(shiftLength * Math.sin(currentDirection+(Math.PI/2.0))));
    }

    public static CoordinatesFloat getShiftVectorFloat(double shiftLength, double currentDirection) {
        return new CoordinatesFloat( shiftLength * Math.cos(currentDirection+(Math.PI/2.0)),
                shiftLength * Math.sin(currentDirection+(Math.PI/2.0)));
    }

//    public static CoordinatesFloat getShiftVectorFloat(double shiftLength, int currentAngleStep) {
//        TrackBuilderHelper
//    }

    public static Coordinates getLengthVector(int length, double factorLength, double currentDirection) {
        return new Coordinates(
                (int)(length * factorLength * Math.cos(currentDirection)),
                (int)(length * factorLength * Math.sin(currentDirection)) );
    }

    public static CoordinatesFloat getLengthVectorFloat(double length, double factorLength, double currentDirection) {
        return new CoordinatesFloat(
                length * factorLength * Math.cos(currentDirection),
                length * factorLength * Math.sin(currentDirection) );
    }

    public static CoordinatesFloat getLengthVectorFloat(double length, double currentDirection) {
        return new CoordinatesFloat(
                length *  Math.cos(currentDirection),
                length *  Math.sin(currentDirection) );
    }
}
