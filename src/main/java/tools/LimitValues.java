package tools;

public class LimitValues {
    private  double minBoxX;
    private  double minBoxY;
    private  double maxLength;

    public LimitValues(double sizeX, double sizeY, double inMaxLength) {
        minBoxX= sizeX;
        minBoxY = sizeY;
        maxLength = inMaxLength;
    }

    public LimitValues(double sizeX, double sizeY) {
        minBoxX= sizeX;
        minBoxY = sizeY;
        maxLength = sizeX > sizeY ? sizeX : sizeY;
    }

    public final double getSurface(){
        return minBoxX * minBoxY;
    }
    public final double getMaxLength(){
        return maxLength;
    }

    //If we can fit in the minBoxSize in both direction, we do fit the max length...
    public boolean fitInBox(double sizeA, double sizeB){
        return (minBoxX <= sizeA && minBoxY <= sizeB || minBoxX <= sizeB && minBoxY <= sizeA);
    }

    public void evaluateValues( LimitValues newLimit, double margins){
        if(getSurface() - margins >= newLimit.getSurface()){
            minBoxY = newLimit.minBoxY;
            minBoxX = newLimit.minBoxX;
        }

        if(maxLength + margins <= newLimit.maxLength){
            maxLength = newLimit.maxLength;
        }
    }

    //Keep only the best values of both!!!!
    public LimitValues(LimitValues previousBest, LimitValues newLimit, double margins){

        if(previousBest.getSurface() - margins < newLimit.getSurface()){
            minBoxY = previousBest.minBoxY;
            minBoxX = previousBest.minBoxX;
        }else{
            minBoxY = newLimit.minBoxY;
            minBoxX = newLimit.minBoxX;
        }

        if(previousBest.maxLength + margins > newLimit.maxLength){
            maxLength = previousBest.maxLength;
        }else{
            maxLength = newLimit.maxLength;
        }
    }
}
