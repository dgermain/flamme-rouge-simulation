package tools;

import trackparts.TrackShape;

import java.util.HashMap;
import java.util.Map;

import static tools.Coordinates.getShiftVectorFloat;

public final class TrackBuilderHelper {


    public double stepToRadian(int steps) {
        return Math.toRadians(22.5*(double)steps);
    }

    Map<Integer, CoordinatesFloat> stepAngleToShapeShift;// = new HashMap<>();

    public Map<TrackShape,Double> shapeChangeDir;// = new HashMap<>();
    public Map<TrackShape,Integer> angleStepChangeDir;// = new HashMap<>();

    private final int baseStraightLength = 192;
    private final int baseTrackWidth = 64;
    private final int baseInnerRadius = 15;
    private final int basetrack45Length = 35;

    private double resizeFactor;

    private double trackWidthFloat;
    private double straightLengthFloat;
    private double innerRadiusFloat;
    private double track45LengthFloat;

    public CoordinatesFloat[][][] nextPtLeftFast;// = new CoordinatesFloat[TrackShape.values().length][16][];
    public CoordinatesFloat[][][] nextPtRightFast;// = new CoordinatesFloat[TrackShape.values().length][16][];
    public CoordinatesFloat[][] shapeEndPointFast;// = new CoordinatesFloat[TrackShape.values().length][16];

    public CoordinatesFloat[][][] nextMidPtLeftFast;
    public CoordinatesFloat[][][] nextMidPtRightFast;



    public TrackBuilderHelper(double newResizeFactor){
        resizeFactor = newResizeFactor;
        nextPtLeftFast = new CoordinatesFloat[TrackShape.values().length][16][];
        nextPtRightFast = new CoordinatesFloat[TrackShape.values().length][16][];
        shapeEndPointFast = new CoordinatesFloat[TrackShape.values().length][16];

        nextMidPtLeftFast = new CoordinatesFloat[TrackShape.values().length][16][];
        nextMidPtRightFast = new CoordinatesFloat[TrackShape.values().length][16][];

        shapeChangeDir = new HashMap<>();
        angleStepChangeDir = new HashMap<>();
        stepAngleToShapeShift = new HashMap<>();

        initializeShapeChangeDir();
    }


    //Instead of calculating each time, I store the direction change for each type of piece.
    //This save time at compute time.
    private void initializeShapeChangeDir(){
        shapeChangeDir.put(TrackShape.LEFT_45,  -(Math.PI / 4));
        shapeChangeDir.put(TrackShape.RIGHT_45, Math.PI / 4);
        shapeChangeDir.put(TrackShape.LEFT_90,  -(Math.PI / 2));
        shapeChangeDir.put(TrackShape.RIGHT_90, Math.PI / 2);
        shapeChangeDir.put(TrackShape.STRAIGHT, 0.0);

        angleStepChangeDir.put(TrackShape.LEFT_45,  -2);
        angleStepChangeDir.put(TrackShape.RIGHT_45, 2);
        angleStepChangeDir.put(TrackShape.LEFT_90,  -4);
        angleStepChangeDir.put(TrackShape.RIGHT_90, 4);
        angleStepChangeDir.put(TrackShape.STRAIGHT, 0);

        //Re-intialize Used Sizes...
        trackWidthFloat = (double)baseTrackWidth * resizeFactor;
        straightLengthFloat = (double)baseStraightLength * resizeFactor;
        innerRadiusFloat = (double)baseInnerRadius * resizeFactor;
        track45LengthFloat = (double)basetrack45Length * resizeFactor;

        for(int i=0;i<16;i++){
            double angleDir = i * (Math.PI / 8);
            stepAngleToShapeShift.put(i, getShiftVectorFloat(trackWidthFloat /2.0, angleDir));
        }

        for(int i=0;i<16;i++) {
            for (TrackShape shape : TrackShape.values()) {
                populateNextPts(new ShapeKey(shape, i));
            }
        }
    }



    private void populateNextPts(ShapeKey shapeKey) {

        CoordinatesFloat position = new CoordinatesFloat(0, 0);

        CoordinatesFloat[] endRight = new CoordinatesFloat[1];
        CoordinatesFloat[] endLeft = new CoordinatesFloat[1];

        int angleStep = shapeKey.stepDir;

        switch (shapeKey.shape) {
            case STRAIGHT:

                CoordinatesFloat endPt = new CoordinatesFloat(
                        straightLengthFloat * Math.cos(stepToRadian(angleStep)),
                        straightLengthFloat * Math.sin(stepToRadian(angleStep)));

                CoordinatesFloat shiftVector = getShiftVectorFloat( (trackWidthFloat / 2.0), stepToRadian(angleStep));

                endRight[0] = endPt.add(shiftVector);
                endLeft[0]  = endPt.subtract(shiftVector);

                shapeEndPointFast[shapeKey.shape.ordinal()][shapeKey.stepDir] = endPt;

                break;

            case LEFT_90:
            case RIGHT_90:
                CoordinatesFloat currentPosition = new CoordinatesFloat(0, 0);

               // int trackSide = (int)(trackWidthFloat);
                double currentDirection = Math.toRadians((double)angleStep * 22.5);//Math.toDegrees(stepToRadian(angleStep));

                CoordinatesFloat shiftVectorA = getShiftVectorFloat(trackWidthFloat/2.0, currentDirection);

                CoordinatesFloat rightStart = currentPosition.add(shiftVectorA);
                CoordinatesFloat leftStart  = currentPosition.subtract(shiftVectorA);

                CoordinatesFloat shiftCenterVector = getShiftVectorFloat(innerRadiusFloat + (trackWidthFloat/2.0), currentDirection);
                CoordinatesFloat rightCenter = currentPosition.add(shiftCenterVector);
                CoordinatesFloat leftCenter  = currentPosition.subtract(shiftCenterVector);

                CoordinatesFloat exteriorStart = null;
                CoordinatesFloat interiorStart = null;
                int turnAngle = 0;
                CoordinatesFloat circleCenter= new CoordinatesFloat(-1110, -1110);
                int midTurn =0;
                boolean rightTurn = true;
                if(shapeKey.shape.equals(TrackShape.LEFT_90)){
                    exteriorStart = rightStart.copy();
                    interiorStart = leftStart.copy();
                    circleCenter = leftCenter;
                    turnAngle = 90;
                    midTurn = 45;
                    //Important for drawing fuction...
                    rightTurn = false;

                }else if(shapeKey.shape.equals(TrackShape.RIGHT_90)){
                    exteriorStart = leftStart.copy();
                    interiorStart = rightStart.copy();
                    circleCenter = rightCenter;
                    turnAngle = -90;
                    midTurn = -45 + 180;
                } else {
                    System.out.println("#########   ERROR, should only be called by a 45 degree turn tile... #######");
                    //return null;
                }

                endRight = new CoordinatesFloat[2];
                endLeft = new CoordinatesFloat[2];


                double turnLength = Math.sqrt(Math.pow(innerRadiusFloat + (trackWidthFloat / 2.0), 2) * 2);

                double exteriorRadius = innerRadiusFloat + trackWidthFloat;
                double middleRadius = innerRadiusFloat +(trackWidthFloat/2.0);
                double interiorRadius = innerRadiusFloat;

                currentDirection = Math.toRadians((double)angleStep * 22.5); //Math.toDegrees(stepToRadian(angleStep));

                CoordinatesFloat midInterior = getCircleAdjust(circleCenter, interiorRadius, currentDirection, midTurn);
                CoordinatesFloat midMiddle   = getCircleAdjust(circleCenter, middleRadius,   currentDirection, midTurn);
                CoordinatesFloat midExterior = getCircleAdjust(circleCenter, exteriorRadius, currentDirection, midTurn);

                CoordinatesFloat endInterior = getCircleAdjust(circleCenter, interiorRadius, currentDirection, turnAngle);
                CoordinatesFloat endMiddle   = getCircleAdjust(circleCenter, middleRadius,   currentDirection, turnAngle);
                CoordinatesFloat endExterior = getCircleAdjust(circleCenter, exteriorRadius, currentDirection, turnAngle);

//                //Compute end
//                CoordinatesFloat mid90Turn = new CoordinatesFloat(
//                        0.5  * turnLength * Math.cos(stepToRadian(angleStep)),
//                        0.5  * turnLength * Math.sin(stepToRadian(angleStep)));

                if(rightTurn) {
                    endRight[0] = midInterior;
                    endLeft[0] = midExterior;
                }else {
                    endRight[0] = midExterior;
                    endLeft[0] = midInterior;
                }
//                if(!inverseMid){
//                    endRight[0] = midExterior;
//                    endLeft[0] = midInterior;
//                }else {
//                    endRight[0] = midInterior;
//                    endLeft[0] = midExterior;
//                }



                //endRight[0] = mid90Turn.add(shiftVector);
                //endLeft[0] = mid90Turn.subtract(shiftVector);


                //Turn 45 degree again
                angleStep = (16 + angleStep + (angleStepChangeDir.get(shapeKey.shape)/2)) & 15;

                //Compute end
                CoordinatesFloat endOf90Turn = new CoordinatesFloat(
                        turnLength * Math.cos(stepToRadian(angleStep)),
                        turnLength * Math.sin(stepToRadian(angleStep)));

                //shiftVector = stepAngleToShapeShift.get(angleStep);
                //endRight[0] = trackVectorToCurbStart.add(shiftVector);
                //endLeft[0] = trackVectorToCurbStart.subtract(shiftVector);


                //Turn 45 degree again
                angleStep = (16 + angleStep + (angleStepChangeDir.get(shapeKey.shape)/2)) & 15;



                shiftVector = stepAngleToShapeShift.get(angleStep);

                endRight[1] = endOf90Turn.add(shiftVector);
                endLeft[1] = endOf90Turn.subtract(shiftVector);

                shapeEndPointFast[shapeKey.shape.ordinal()][shapeKey.stepDir] = endOf90Turn;
                break;

            case LEFT_45:
            case RIGHT_45:
                endRight = new CoordinatesFloat[2];
                endLeft = new CoordinatesFloat[2];
                //

               // int track45LengthFloat = 35;

                CoordinatesFloat trackVectorToCurbStart = new CoordinatesFloat(
                        track45LengthFloat * Math.cos(stepToRadian(angleStep)),
                        track45LengthFloat * Math.sin(stepToRadian(angleStep)));

                angleStep = (16 + angleStep + (angleStepChangeDir.get(shapeKey.shape) / 2)) & 15;
                //Get side points//
                shiftVector = stepAngleToShapeShift.get(angleStep);
                endRight[0] = trackVectorToCurbStart.add(shiftVector);
                endLeft[0] = trackVectorToCurbStart.subtract(shiftVector);

                //Turn full 45 degree and go to end point
                angleStep = (16 + angleStep + (angleStepChangeDir.get(shapeKey.shape)/2)) & 15;

                CoordinatesFloat trackVectorToCurbEnd = new CoordinatesFloat(
                        track45LengthFloat * Math.cos(stepToRadian(angleStep)),
                        track45LengthFloat * Math.sin(stepToRadian(angleStep)));

                position = trackVectorToCurbStart.add(trackVectorToCurbEnd);

                shiftVector = stepAngleToShapeShift.get(angleStep);

                endRight[1] = position.add(shiftVector);
                endLeft[1] = position.subtract(shiftVector);

                shapeEndPointFast[shapeKey.shape.ordinal()][shapeKey.stepDir] = position;

                break;
        }

        //For each shape - original direction, set next end point position.
        nextPtLeftFast[shapeKey.shape.ordinal()][shapeKey.stepDir] = endLeft;
        nextPtRightFast[shapeKey.shape.ordinal()][shapeKey.stepDir] = endRight;
    }


    private CoordinatesFloat getCircleAdjust(CoordinatesFloat circleCenter, double radius, double direction, double angle) {

        CoordinatesFloat deltaCoord = getCircleAdjust(radius, direction, angle);

        CoordinatesFloat answer = new CoordinatesFloat(circleCenter.x+ deltaCoord.x, circleCenter.y+ deltaCoord.y);
        return answer;
    }

    private CoordinatesFloat getCircleAdjust(double radius, double direction, double angle) {

        double radAngle = Math.toRadians(angle);
        CoordinatesFloat circleAdjust = new CoordinatesFloat(Math.sin(radAngle) * radius, Math.cos(radAngle) * radius);

        double cosAngle = Math.cos(direction );//+ Math.toRadians(90));
        double sinAngle = Math.sin(direction );//+ Math.toRadians(90));

        double xRot = cosAngle * circleAdjust.x - sinAngle * circleAdjust.y;
        double yRot = sinAngle * circleAdjust.x + cosAngle * circleAdjust.y;

        CoordinatesFloat adjustment = new CoordinatesFloat(xRot,yRot);

        return adjustment;
    }

}
