package tools;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FileTools {

    public static ArrayList<String> readTrackFile(String trackFile){

        ArrayList<String> allTracks = new ArrayList<>();

        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(trackFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String line = null;
        try {
            line = br.readLine();

            while (line != null) {
                line = line.trim();

                //Skip comment, or empty line
                if(line.length() != 0) {
                    String track = ExtractLine(line);
                    if(track.length() > 0)
                        allTracks.add(track);
                }

                line = br.readLine();
            }

            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return allTracks;
    }

    private static String ExtractLine(String line) {
        String[] subParts = line.split("\t");
        if(subParts.length == 2){
            return subParts[1].trim();
        }
        return "";
    }
}
