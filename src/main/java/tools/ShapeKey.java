package tools;

import trackparts.TrackShape;

/**
 * Created by davidgermain on 2018-02-15.
 */
public class ShapeKey {
    public TrackShape shape;
    public int stepDir;

    public ShapeKey(TrackShape sh, int dir){
        shape = sh;
        stepDir = dir;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ShapeKey)) return false;

        ShapeKey shapeKey = (ShapeKey) o;

        if (stepDir != shapeKey.stepDir) return false;
        return shape == shapeKey.shape;
    }

    @Override
    public int hashCode() {
        int result = shape.hashCode();
        result = 31 * result + stepDir;
        return result;
    }
}
