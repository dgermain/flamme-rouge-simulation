package tools;

import gui.DrawingTools;

import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;

/**
 * Created by davidgermain on 2018-01-23.
 */
public class LineCrossingChecker {

    private boolean isValid = true;

    ArrayList<CoordinatesFloat> trackToDateRight = new ArrayList<>();
    ArrayList<CoordinatesFloat> trackToDateLeft = new ArrayList<>();

    public ArrayList<CoordinatesFloat> getAllCoordinates(){
        ArrayList<CoordinatesFloat> tempCoord = new ArrayList<>();
        for(CoordinatesFloat c: trackToDateRight){
            tempCoord.add(c.copy());
        }
        for(CoordinatesFloat c: trackToDateLeft){
            tempCoord.add(c.copy());
        }
        return tempCoord;
    }

    public boolean isValid(){ return  isValid;}

    private LineCrossingChecker(){}
    public LineCrossingChecker(CoordinatesFloat endPointRight,CoordinatesFloat endPointLeft){
        trackToDateRight.add(endPointRight.copy());
        trackToDateLeft.add(endPointLeft.copy());
    }

    public boolean addEndPoint(CoordinatesFloat endPointRight,CoordinatesFloat endPointLeft){
        if(!isValid) {
            return false;
        }

        isValid = evaluatePotentialEndPoint(endPointRight, endPointLeft);

        trackToDateRight.add(endPointRight.copy());
        trackToDateLeft.add(endPointLeft.copy());

        return isValid;
    }

    /**
     * This function is used to avoid adding a segment if it would cause a crossing...
     * @param endPointRight, endPointLeft
     * @return If the point was succesfully added or not
     */
    public boolean addIfValid(CoordinatesFloat endPointRight,CoordinatesFloat endPointLeft){

        if(evaluatePotentialEndPoint(endPointRight, endPointLeft)){
            trackToDateRight.add(endPointRight.copy());
            trackToDateLeft.add(endPointLeft.copy());
            return true;
        }

        //The segment was not added.
        return false;
    }

    public boolean evaluatePotentialEndPoint(CoordinatesFloat pointRight, CoordinatesFloat pointLeft){

        if(trackToDateRight.size() <= 1){
            return isValid;
        }

        CoordinatesFloat startPtRight = trackToDateRight.get(trackToDateRight.size()-1);
        CoordinatesFloat startPtLeft  = trackToDateLeft.get(trackToDateLeft.size()-1);


        //Check crossing of start Line, this is perpendicular to other lines !!!
        boolean haveCrossing = areSegmentCrossing(startPtRight, pointRight , trackToDateLeft.get(0), trackToDateRight.get(0));
         haveCrossing |= areSegmentCrossing(startPtLeft, pointLeft , trackToDateLeft.get(0), trackToDateRight.get(0));


        for(int i = 0; i < trackToDateRight.size() -2; ++i ){
            CoordinatesFloat midTemp = startPtRight.midpoint(pointRight);
            CoordinatesFloat midTemp2 = trackToDateLeft.get(i).midpoint(trackToDateLeft.get(i+1));

            double limitCross = midTemp.sqDist(pointRight) + midTemp2.sqDist(trackToDateLeft.get(i)) +1.0;


            if(midTemp.subtract(midTemp2).GetSqrtLength() > limitCross) // 193 * 193 (being the max dist between two line)
                continue;


            //haveCrossingLeftLeft ?
            haveCrossing |= areSegmentCrossing(startPtRight, pointRight , trackToDateLeft.get(i), trackToDateLeft.get(i+1));
            //haveCrossingRightRight
            haveCrossing |=  areSegmentCrossing(startPtLeft, pointLeft  , trackToDateRight.get(i), trackToDateRight.get(i+1));

            //haveCrossingLeftRight
            haveCrossing |= areSegmentCrossing(startPtRight, pointRight , trackToDateRight.get(i), trackToDateRight.get(i+1));

            //haveCrossingRightLeft
            haveCrossing |= areSegmentCrossing(startPtLeft, pointLeft  , trackToDateLeft.get(i), trackToDateLeft.get(i+1));


            if(haveCrossing){
                return false;
            }
        }

        return isValid;
    }

    Line2D tempLine1 = new Line2D.Float(0,0,0,0);
    Line2D tempLine2 = new Line2D.Float(0,0,0,0);

    private boolean areSegmentCrossing(CoordinatesFloat startPtOne, CoordinatesFloat endPtOne, CoordinatesFloat startPTwo, CoordinatesFloat endPtTwo) {

        //This function checks if 2 lines are crossing.
        //Line2D line1 = new Line2D.Float((int)startPtOne.x , (int)startPtOne.y, (int)endPtOne.x, (int)endPtOne.y);
        //Line2D line2 = new Line2D.Float((int)startPTwo.x, (int) startPTwo.y, (int)endPtTwo.x, (int)endPtTwo.y);

        tempLine1.setLine(startPtOne.x, startPtOne.y, endPtOne.x, endPtOne.y);
        tempLine2.setLine(startPTwo.x, startPTwo.y, endPtTwo.x, endPtTwo.y);

        //return line2.intersectsLine(line1);
        return tempLine2.intersectsLine(tempLine1);
    }


    public void drawLines(Graphics2D g2d, CoordinatesFloat startPt) {

        if(trackToDateRight.size() != trackToDateLeft.size())
            System.out.println("error of size....");

        //System.out.println("Size R, L : " + trackToDateRight.size() + " : " + trackToDateLeft.size());

        for(int i=0;i<trackToDateRight.size()-1;++i){
            //System.out.println(i + " : " + trackToDateRight.get(i).x + ", " + trackToDateRight.get(i).y);
            Color backgroundColor = new Color(0xFF090);// new Color(0xD6D6D6);
            g2d.setColor(new Color(backgroundColor.getRGB()));

            //System.out.println(i + " ->R " + trackToDateRight.get(i).x + " , " + trackToDateRight.get(i).y);
            //System.out.println(i + "+1 ->R " + trackToDateRight.get(i+1).x + " , " + trackToDateRight.get(i+1).y);

            Coordinates rightA = trackToDateRight.get(i).toCoordinates();
            Coordinates rightB = trackToDateRight.get(i+1).toCoordinates();
            drawLine(g2d,rightA,rightB);
            //drawLine(g2d,startPt.toCoordinates(),rightB);


            g2d.setColor(new Color(0x0000FF));
            Coordinates leftA = trackToDateLeft.get(i).toCoordinates();
            Coordinates leftB = trackToDateLeft.get(i+1).toCoordinates();
            drawLine(g2d,rightA,leftA);

            Point origin = new Point(rightA.x,rightA.y);
            Point leftPt = new Point(leftA.x,leftA.y);

            DrawingTools.drawCircle(g2d, origin, 5, true, true,  new Color(0x000000).getRGB(), 0);
            DrawingTools.drawCircle(g2d, leftPt, 5, true, true,  new Color(0x000000).getRGB(), 0);

            backgroundColor = new Color(0x906080);// new Color(0xD6D6D6);
            g2d.setColor(new Color(backgroundColor.getRGB()));

            //System.out.println(i + " ->L " + trackToDateLeft.get(i).x + " , " + trackToDateLeft.get(i).y);
            //System.out.println(i + "+1 ->L " + trackToDateLeft.get(i+1).x + " , " + trackToDateLeft.get(i+1).y);

            drawLine(g2d,leftA,leftB);

        }
    }

    private void drawLine(Graphics2D g2d, Coordinates rightA, Coordinates rightB) {
        g2d.drawLine(rightA.x,rightA.y,rightB.x,rightB.y);

    }

    public void dropEndPoint() {
        trackToDateRight.remove(trackToDateRight.size()-1);
        trackToDateLeft.remove(trackToDateLeft.size()-1);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public LineCrossingChecker copy() {
        LineCrossingChecker copy = new LineCrossingChecker();
        copy.isValid = this.isValid;

        copy.trackToDateRight = new ArrayList<>();
        copy.trackToDateLeft = new ArrayList<>();

        for(CoordinatesFloat coor: this.trackToDateRight){
            copy.trackToDateRight.add(coor.copy());
        }
        for(CoordinatesFloat coor: this.trackToDateLeft){
            copy.trackToDateLeft.add(coor.copy());
        }

        return copy;
    }
}
