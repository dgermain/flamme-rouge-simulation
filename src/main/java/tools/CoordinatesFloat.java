package tools;

/**
 * Created by davidgermain on 2018-01-23.
 */
public class CoordinatesFloat {
    public double x;
    public double y;

    public CoordinatesFloat(double x, double y){
        this.x = x;
        this.y = y;
    }

    public double GetSqrtLength() {
        return this.x *this.x + this.y* this.y;
    }

    public CoordinatesFloat add(CoordinatesFloat shiftPosition) {
        return  new CoordinatesFloat(x + shiftPosition.x, y + shiftPosition.y);
    }

    public CoordinatesFloat midpoint(CoordinatesFloat shiftPosition) {
        return new CoordinatesFloat((x + shiftPosition.x)/2.0, (y + shiftPosition.y)/2.0);
    }

    public double sqDist(CoordinatesFloat otherPos) {
        return  (otherPos.x -x) * (otherPos.x -x) + (otherPos.y -y) * (otherPos.y -y);

    }



    public CoordinatesFloat subtract(CoordinatesFloat shiftPosition) {
        return new CoordinatesFloat(x - shiftPosition.x, y - shiftPosition.y);
    }

    public CoordinatesFloat copy() {
        return  new CoordinatesFloat(x, y);
    }

    public static CoordinatesFloat getShiftVector(double shiftLength, double currentDirection) {
        return new CoordinatesFloat( shiftLength * Math.cos(currentDirection+(Math.PI/2.0)),
                shiftLength * Math.sin(currentDirection+(Math.PI/2.0)));
    }

    public static CoordinatesFloat getLengthVector(double length, double factorLength, double currentDirection) {
        return new CoordinatesFloat(
                length * factorLength * Math.cos(currentDirection),
                length * factorLength * Math.sin(currentDirection) );
    }

    public Coordinates toCoordinates() {
        return new Coordinates((int)x, (int)y);
    }

    @Override
    public String toString() {
        return x + "\t" + y;
    }
}
