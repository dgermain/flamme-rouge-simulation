package tools;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class HistogramBins {
    double minLimit = 0.0;
    double maxLimit = 0.0;

    private final long[] result;
    private final double binSize;
    private final int numBins;

    private long underCount = 0;
    private long overCount = 0;

    private double maxOver = 0;
    private double minUnder = 0;

    public HistogramBins(double min, double max, int numberOfBins){
        numBins = numberOfBins;

        result = new long[numBins];
        binSize = (max - min)/numBins;

        minLimit = min;
        maxLimit = max;

        maxOver = max;
        minUnder = min;
    }

    public void addPoint(double dataPoint) {
        int bin = (int) ((dataPoint - minLimit) / binSize); // changed this from numBins

        //Make sure the bin is in the values, otherwise, put at first or last bin.
        bin = Math.max(0,Math.min(numBins-1, bin));

        result[bin]++;

        //Skip the under/over count....

        /*

        if (bin < 0) {
            //this data is smaller than min
            if (dataPoint < minUnder)
                minUnder = dataPoint;
            underCount++;
            //Classify as First Bin
            result[0]++;
        }
        else if (bin >= numBins) {
            //this data point is bigger than max
            overCount++;
            if(dataPoint>maxOver){
                maxOver = dataPoint;
            }

            //Classify as Last Bin
            result[numBins-1]++;
        }
        else {
            result[bin]++;
        }
        */
    }

    //Get the answer to the data.
    public long[] getResults(){
        return result;
    }

    public void printOverflow(){
        System.out.println("Values Count. UnderCount: " + underCount + " OverCount: " + overCount);
        System.out.println("Extreme Values. MinUnder: " + minUnder + " MaxOver: " + maxOver);
    }

    public void writeToFile(String filename){
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            for(int i=0;i<numBins;++i){
                double val =  minLimit + (i*binSize);
                writer.write(val + "\t" + result[i] + "\n");
            }

            writer.write("\n\n");
            writer.write("Values Count. UnderCount: " + underCount + " OverCount: " + overCount + "\n");
            writer.write("Extreme Values. MinUnder: " + minUnder + " MaxOver: " + maxOver + "\n");


        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
