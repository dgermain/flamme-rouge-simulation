package Player;

import playerparts.CardDeck;
import playerparts.RacerType;

/**
 * Created by davidgermain on 2018-01-13.
 */
public abstract class PlayerInstance {

    public RacerType racer = RacerType.sprinter;

    //Need two decks
    CardDeck sprinterDeck = null;
    CardDeck rouleurDeck = null;

    int selectedSprinterCard = 0;
    int selectedRouleurCard = 0;

    //Select Cards (GameTrack)

    public abstract void selectCards();

    public  int  getRouteurCard() {
        int temp= selectedRouleurCard;
        selectedRouleurCard = 0;
        return temp;
    }
    public  int  getSprinterCard(){
        int temp= selectedSprinterCard;
        selectedSprinterCard = 0;
        return temp;
    }


    public void initSprinterDeck(int cardCount[]){
        sprinterDeck.initializeDeck(cardCount);
    }

    public void initRouleurDeck(int cardCount[]){
        rouleurDeck.initializeDeck(cardCount);
    }

    public void addRouleurFatigue(){
        rouleurDeck.addFatigueCard();
    }

    public void addSprinterFatigue(){
        sprinterDeck.addFatigueCard();
    }

    public int getCyclistCard(RacerType racerType) {
        if(racerType == RacerType.rouleur)
            return selectedRouleurCard;
        else
            return selectedSprinterCard;
    }
}
