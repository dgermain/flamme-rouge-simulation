import trackparts.RoadType;
import trackparts.TrackInfo;
import trackparts.TrackShape;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//This class purpose is to pick a track path, then verify all flip possibility
//Build elevation profile for the track.
//Use hashmap to check for collision.

//Then do this twice, and check unique elevation overlap.


public class TrackElevationExplorer {

    RaceTrackGenerator trackGenerator;

    public static void main(String[] args ){
        TrackElevationExplorer elevationExplorer = new TrackElevationExplorer();

        elevationExplorer.elevationSearch();
    }

    public TrackElevationExplorer(){

        trackGenerator = new RaceTrackGenerator(true, true, true);
    }

    private void elevationSearch() {
        String randomTrack = trackGenerator.generateRandomTrack();
        System.out.println(randomTrack);

        randomTrack = "AcpODbJnhqIsFkgrEMtLu";//"AFseroQgTDHPJklMciBNU";

        HashMap<String, Integer> elevationHash = getAllElevation(randomTrack);

        System.out.println("Found: " + elevationHash.size() + " different elevation profile");

        for (int i = 0; i < 100; ++i) {
            String secondRandomTrack = trackGenerator.generateRandomTrack();

            if (i < 19) {
                secondRandomTrack = "a";
                char startChar = 'a';
                for (int j = 1; j < 20; j++) {
                    int next = i + j;
                    if (next >= 20) {
                        next = next - 20 + 1;
                    }
                    char nextChar = (char) (startChar + next);

                    secondRandomTrack += nextChar;
                }
                secondRandomTrack += "U";
            } else {
               // break;
            }
            System.out.println(secondRandomTrack);




            HashMap<String, Integer> secondElevationHash = getAllElevation(secondRandomTrack);

            //System.out.println("Found: " + secondElevationHash.size() + " different elevation profile");

            int collisionCount =0;
            for (String key : secondElevationHash.keySet()) {
                if (elevationHash.containsKey(key)) {
                    elevationHash.put(key, elevationHash.get(key) + secondElevationHash.get(key));
                    //System.out.println(i + " - Count: " + elevationHash.get(key) + " for elevation: " + key);
                    collisionCount++;
                } else {
                    elevationHash.put(key, secondElevationHash.get(key));
                }
            }

            System.out.println(i + " - Found: " + elevationHash.size() + " combined different elevation profile - " + collisionCount + " collisions count - RaceTrack: "+secondRandomTrack);
        }
    }

    private String getElevationProfile(String raceString){
        ArrayList<RoadType> segmentType = new ArrayList<>();
        ArrayList<Integer> segmentLength = new ArrayList<>();

        int roadTotalLength = 0;
        for(char c: raceString.toCharArray()){
            TrackInfo info = trackGenerator.getTrackInfo(Character.toString(c));
            //System.out.println(info.toString());

            for(int i=0; i< info.getTypeSize(); ++i){
                RoadType roadType = info.getRoadType(i);
                int roadLength = info.getTrackSegmentLength(i);

                //System.out.println("Segment: " + c + " Type: " + roadType.toString());

                if(segmentType.isEmpty() || segmentType.get(segmentType.size()-1) != roadType){
                    segmentType.add(roadType);
                    segmentLength.add(roadLength);
                }else {
                    segmentLength.set(segmentType.size()-1, roadLength + segmentLength.get(segmentLength.size()-1));
                }
                roadTotalLength+=roadLength;
            }
        }
        //System.out.println("TotalRoad Length: "+ roadTotalLength);
        String elevationProfileStr = roadToString(segmentType, segmentLength);
        //System.out.println(elevationProfileStr);

        return elevationProfileStr;
    }

    private HashMap<String,Integer> getAllElevation(String trackString){
        HashMap<String,Integer> elevationHash = new HashMap<>();

        //int maxTest = 5;//trackString.length()
        int maxTest = trackString.length();
        long maxPermutation = Math.round(Math.pow(2, maxTest));

        //System.out.println("trackString: " + trackString );
        //System.out.println("Testing: " + maxPermutation + " Total Permutations");

        for(long permutation=0; permutation < maxPermutation; ++permutation){
            //System.out.println("permutation:" + permutation);
            String testStr = "";
            for(int j=0; j < trackString.length();++j) {
                int item = trackString.length() -1 -j;
                long comp = Math.round(Math.pow(2,item));
                //System.out.println("Comp:" + comp);

                if ((permutation & comp) > 0) {
                    testStr = testStr + Character.toLowerCase(trackString.charAt(j));
                }else{
                    testStr = testStr + Character.toUpperCase(trackString.charAt(j));
                }
            }
            System.out.println("SideSwitching\t" + testStr);
            if(permutation>500)
                break;
            String localProfile = getElevationProfile(testStr);
            if(elevationHash.containsKey(localProfile)){
                elevationHash.put(localProfile, elevationHash.get(localProfile)+1);
            }else{
                elevationHash.put(localProfile,1);
            }
        }

        return elevationHash;
    }

    private String roadToString(ArrayList<RoadType> segmentType, ArrayList<Integer> segmentLength) {
        String roadStr = "";
        for(int i=0 ; i< segmentType.size();++i){
            roadStr += segmentLength.get(i);

            switch(segmentType.get(i)){
                case PRESTART:
                    roadStr += "S"; break;
                case UP:
                    roadStr += "U"; break;
                case DOWN:
                    roadStr += "D"; break;
                case FLAT:
                    roadStr += "F"; break;
                case FINISH:
                    roadStr += "E"; break;
            }


        }

        return roadStr.substring(0,roadStr.length());
    }


}
