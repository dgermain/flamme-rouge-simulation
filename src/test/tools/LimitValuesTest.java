package tools;

import static org.junit.jupiter.api.Assertions.*;

class LimitValuesTest {

    @org.junit.jupiter.api.Test
    void getSurface() {
        LimitValues limits = new LimitValues(10,20);

        assertEquals(limits.getSurface(), 200.0);

        LimitValues limitInversed = new LimitValues(20,10);

        assertEquals(limitInversed.getSurface(), 200.0);

    }

    @org.junit.jupiter.api.Test
    void getMaxLength() {
        LimitValues limits = new LimitValues(10,20);

        assertEquals(limits.getMaxLength(), 20.0);

        LimitValues limitsInverse = new LimitValues(20,10);

        assertEquals(limitsInverse.getMaxLength(), 20.0);

    }

    @org.junit.jupiter.api.Test
    void fitInBox() {
        LimitValues limits = new LimitValues(10,20);

        //Does the limits fits in the given box Size... In Both presented dimensions
        assertTrue(limits.fitInBox(10.0,20.0));
        assertTrue(limits.fitInBox(20.0,10.0));

        LimitValues limitsSmall = new LimitValues(11.0,11.0);
        //The Limits should not fit given the max Length is greater than one dimension.
        assertFalse(limitsSmall.fitInBox(10.0,20.0));
        assertFalse(limitsSmall.fitInBox(20.0,10.0));
    }
}