import org.junit.jupiter.api.Test;

import java.util.PriorityQueue;

import static org.junit.jupiter.api.Assertions.*;

class ShapeStatsKeeperTest {

    @Test
    void createPriorityQueue() {
        ShapeStatsKeeper stats = new ShapeStatsKeeper(3,3,3, true);

        PriorityQueue<ShapeStatsKeeper.TrackPair> priority =  stats.createPriorityQueue(10, true);

        for(int i=0;i< 100;++i){
            ShapeStatsKeeper.TrackPair pair = new ShapeStatsKeeper.TrackPair(i + "" , i);
            ShapeStatsKeeper.add(10, priority, pair);
        }

        for(ShapeStatsKeeper.TrackPair p: priority){
            //System.out.println("biggest Surface : " + p.trackScore + " for " + p.trackString);
        }

    }
}